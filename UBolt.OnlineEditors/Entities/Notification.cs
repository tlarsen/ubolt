﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UBolt.OnlineEditors.Interfaces;

namespace UBolt.OnlineEditors.Entities
{
    public class Notification : INotify
    {
        public string Title
        {
            get;
            set;
        }

        public string Content
        {
            get;
            set;
        }

        public string Duration
        {
            get;
            set;
        }
    }
}