﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UBolt.OnlineEditors.Interfaces
{
    interface INotify
    {
        string Title { get; set; }
        string Content { get; set; }
        string Duration { get; set; }
    }
}
