﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UBolt.OnlineEditors.Entities;

namespace UBolt.OnlineEditors.Interfaces
{
    interface IOnlineEditorsService
    {
        UmbracoUser DisconnectEditor(string clientId);
        UmbracoUser ConnectEditor(string clientId);
        
        UmbracoBackofficePage AddPage(UmbracoUser editor, string pageName);
        void EnterPage(UmbracoUser editor, UmbracoBackofficePage page);
        void LeavePage(UmbracoUser editor, UmbracoBackofficePage page);
    }
}
