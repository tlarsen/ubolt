﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;

namespace UBolt.OnlineEditors.Utility
{
    public static class UmbracoClientPreApplicationStart
    {
        private static bool _httpModuleRegistered;

        public static void RegisterModule()
        {
            if (_httpModuleRegistered)
                return;
            DynamicModuleUtility.RegisterModule(typeof(UmbracoClientModule));
            _httpModuleRegistered = true;
        }

    }
}