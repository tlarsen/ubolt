﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using ClientDependency.Core;
using ClientDependency.Core.Controls;


namespace UBolt.OnlineEditors.Utility
{
    internal static class UmbracoClientResource
    {

        internal static void AddClientFiles(Page page)
        {
            AddStyleSheets(page);
            AddJavascripts(page);
        }
        
        private static void AddStyleSheets(Page page)
        {
            string webResourceUrl = GetWebResourceUrl("UBolt.OnlineEditors.UI.Css.onlineEditors.css");
            AddToClientDependency(page, webResourceUrl, (ClientDependencyType)1, 96, "", (string)null);  
        }

        private static void AddJavascripts(Page page)
        {
            string webResourceUrl = GetWebResourceUrl("UBolt.OnlineEditors.UI.Javascripts.onlineEditors.js");
            AddToClientDependency(page, webResourceUrl, (ClientDependencyType)1, 96, "", (string)null);
        }

        private static void AddToClientDependency(Page page, string url, ClientDependencyType cdType, int priority = 0, string idTag = "", string pathNameAlias = null)
        {
            string str = page.Server.HtmlEncode(url);
            idTag = idTag.Replace(" ", "").Replace(".", "-");
            switch ((int)cdType)
            {
                case 0:
                    JsInclude jsInclude1 = new JsInclude();
                    jsInclude1.ID = idTag;
                    jsInclude1.FilePath = str;
                    jsInclude1.Priority = priority;

                    JsInclude jsInclude2 = jsInclude1;
                    if (pathNameAlias != null)
                        jsInclude2.PathNameAlias = pathNameAlias;
                    try
                    {
                        page.Header.Controls.Add(jsInclude2);
                        break;
                    }
                    catch
                    {
                        throw new Exception("js Inclue fail");
                        page.Controls.Add(jsInclude2);
                        break;
                    }
                case 1:
                    CssInclude cssInclude1 = new CssInclude();
                    cssInclude1.ID = idTag;
                    cssInclude1.FilePath = str;
                    cssInclude1.Priority = priority;

                    CssInclude cssInclude2 = cssInclude1;
                    if (pathNameAlias != null)
                        cssInclude2.PathNameAlias = pathNameAlias;
                    try
                    {
                        page.Header.Controls.Add(cssInclude2);
                        break;
                    }
                    catch
                    {
                        throw new Exception("css Inclue fail");
                        page.Controls.Add(cssInclude2);
                        break;
                    }
            }
        }

        private static string GetWebResourceUrl(string resourceName)
        {
            return new Page().ClientScript.GetWebResourceUrl(typeof(UmbracoClientResource), resourceName);
        }
    }
}