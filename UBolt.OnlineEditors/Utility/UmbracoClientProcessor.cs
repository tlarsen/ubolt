﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using UBolt.OnlineEditors.Services;
using umbraco;
using umbraco.BasePages;
using umbraco.cms.presentation;

namespace UBolt.OnlineEditors.Utility
{
    internal static class UmbracoClientProcessor
    {
        internal static void AddClientFiles(HttpApplication context)
        {
            IHttpHandler currentHandler = context.Context.CurrentHandler;
            if (currentHandler is _Default)
            {
                string str = string.Format("{0}/umbraco.aspx", (object)GlobalSettings.ClientPath);
                try
                {
                    context.Response.Redirect(str);
                }
                catch
                {
                    context.Server.Transfer(str);
                }
            }
            if (!(currentHandler is _umbraco) && !(currentHandler is UmbracoEnsuredPage))
                return;
            Page page = currentHandler as Page;
            page.Load += (EventHandler)((s1, e1) => UmbracoClientResource.AddClientFiles(page));
           
            //if (currentHandler is _umbraco)
            //    page.Load += (EventHandler)((s1, e1) => Resource.AddJQueryCreateScript(page));

           
        }
    }
}