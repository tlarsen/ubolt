﻿using System;
using System.Web;

namespace UBolt.OnlineEditors.Utility
{
    public class UmbracoClientModule : IHttpModule
    {

        public void Dispose()
        {
            
        }

        public void Init(HttpApplication context)
        {
            context.PostMapRequestHandler += (EventHandler)((sender, eventArg) => UmbracoClientProcessor.AddClientFiles(context));
            //context.BeginRequest += (EventHandler)((s, e) => UmbracoClientProcessor.HandleNoteRequests(application));
        }
    }
}