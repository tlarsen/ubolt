﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Web;
using UBolt.OnlineEditors.Entities;
using UBolt.OnlineEditors.Hubs;
using SignalR;

namespace UBolt.OnlineEditors.Services
{
    public class NotificationService
    {
        private static readonly Lazy<NotificationService> _instance = new Lazy<NotificationService>(() => new NotificationService());
        private static readonly ConcurrentDictionary<string, Notification> _notifications = new ConcurrentDictionary<string, Notification>();

        private NotificationService()
        {

        }

        public static NotificationService Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        public IEnumerable<Notification> GetAllNotifications()
        {
            return _notifications.Values;
        }

        public void NotifyGroup(object notification)
        {
            GetClients().add(notification);
        }

        private static dynamic GetClients()
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<NotificationHub>();
            return context.Clients;
        }



    }
}