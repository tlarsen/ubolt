﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Web;
using UBolt.OnlineEditors.Entities;
using UBolt.OnlineEditors.Hubs;
using SignalR;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.member;

namespace UBolt.OnlineEditors.Services
{
    public class UmbracoUserService
    {
        private static readonly Lazy<UmbracoUserService> _instance = new Lazy<UmbracoUserService>(() => new UmbracoUserService());
        private static readonly ConcurrentDictionary<string, UmbracoUser> _onlineEditor = new ConcurrentDictionary<string, UmbracoUser>();

        private UmbracoUserService()
        {
            
        }

        public static UmbracoUserService Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        public void AddOnlineEditor(string name)
        {
            _onlineEditor.TryAdd(name ,new UmbracoUser() { Name = name, currentPage = new Random().Next(1,3) });
            
            //Callback method here ! 
            GetEditors().notifyEditorLoggedOn(name);
            this.NotifyEditors(new UmbracoUser() { Name = "test" }, "test");
        }

        public bool IsEdiable(int pageID, string userID)
        {
            return GetOnlineEditors().Any(x => x.currentPage == pageID && x.Name == userID);
        }

        public IEnumerable<UmbracoUser> GetOnlineEditors()
        {
            return _onlineEditor.Values;
        }

        public IEnumerable<UmbracoUser> GetOnlineEditors(int pageID)
        {
            return _onlineEditor.Values.Where(x => x.currentPage == pageID);
        }

        public void NotifyEditors(object notification)
        {
            GetEditors().notifyEditors(notification);
        }
        public void NotifyEditors(object notification, string groupID)
        {
            GetEditors(groupID).notifyEditors(notification);
        }

        private static dynamic GetEditors()
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<UmbracoEditorHub>();
            return context.Clients;
        }
        private static dynamic GetEditors(string groupID)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<UmbracoEditorHub>();
            return context.Clients[groupID];
        }

        #region Umbraco 

        public User GetUser()
        {
            return User.GetCurrent();;
        }


        #endregion

    }
}