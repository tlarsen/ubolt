﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SignalR.Hubs;
using UBolt.OnlineEditors.Services;
using UBolt.OnlineEditors.Entities;

namespace UBolt.OnlineEditors.Hubs
{
    [HubName("umbracoEditorHub")]
    public class UmbracoEditorHub : Hub
    {
        private readonly UmbracoUserService _service;

        public UmbracoEditorHub() : this(UmbracoUserService.Instance) { }
        public UmbracoEditorHub(UmbracoUserService service)
        {
            _service = service;
        }

        public void AddOnlineEditor(string name)
        {

            _service.AddOnlineEditor(name);
            this.Groups.Add(Context.ConnectionId, "test");
        }

        public IEnumerable<UmbracoUser> GetOnlineEditors()
        {
            return _service.GetOnlineEditors();
        }

        public IEnumerable<UmbracoUser> GetOnlineEditors(int pageID)
        {
            return _service.GetOnlineEditors(pageID);
        }

        public bool IsEdiable(int pageID, string name)
        {
            return _service.IsEdiable(pageID, name);
        }

    }
}