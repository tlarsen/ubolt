﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SignalR.Hubs;
using UBolt.OnlineEditors.Services;
using UBolt.OnlineEditors.Entities;

namespace UBolt.OnlineEditors.Hubs
{
    [HubName("notificationHub")]
    public class NotificationHub : Hub
    {
        private readonly int TimeoutInSeconds = 30;
        private readonly NotificationService _service;

        public NotificationHub() : this(NotificationService.Instance) { }

        public NotificationHub(NotificationService service)
        {
            _service = service;
        }

        public void Join(string myName)
        {
            Clients.addNotification(myName);
        }
        
        public void AddToGroup(string group)
        {
            this.Groups.Add(Context.ConnectionId, group);
        }

        public IEnumerable<Notification> GetAllNotifications()
        {
            return _service.GetAllNotifications();
        }

        public void NotifyGroup(object notification)
        {
            _service.NotifyGroup(notification);
        }


    }
}