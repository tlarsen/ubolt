﻿var selectedFlexComponents = new Array();
var activeFlexComponent = null;


$(document).ready(function () {
    
    //init slider 
    $('.slider').slider({
        min: 1,
        max: 12,
        step: 1,
        slide: function (event, slider) {
            
            var cssClass = "grid_" + slider.value + " component selected";
            var component = $(".container").find("[data-id='" + activeFlexComponent.data("id") + "']");
            component.attr("class", cssClass);

            $(".container").find("[data-id='" + activeFlexComponent.data("id") + "']").data("columns", slider.value);
            

        },
        stop: function (event, slider) {
            console.log('piik');
            SerializeComponents();
            
        }
    });

    //sort
    $(".container").sortable({ opacity: 0.8, helper: "original" });
    $(".container").disableSelection();


    //event handlers 
    $('.container').find("[data-id]").live('click', function (e) {
       
        activeFlexComponent = $(this);
        $(".slider").slider("option", "value", activeFlexComponent.data("columns"));
        
        var component = $(".container").find("[data-id='" + activeFlexComponent.data("id") + "']");
        var cssClass = "grid_" + activeFlexComponent.data("columns") + " component selected";
        component.attr("class", cssClass);
        


    });
    
});


//Tree 
function onNodeClick(nodeObject) {
    console.log('debug');
    appendComponent(nodeObject);
    SerializeComponents();
    
}

function appendComponent(nodeObject) {

    var container = $('#componentContainer');
    var componentToInsert = $("<div data-id='"+ nodeObject +"' data-columns='3' class='grid_3 component'>" +
        "<a href='javascript:void(0);' class='info'>fjern</a>" +
        "<div class='inner'>Component" +
        "</div></div>");

    container.append(componentToInsert);
    
}

function SerializeComponents() {
    
    selectedFlexComponents.length = 0;
    $('.container').find("[data-id]").each(function () {

        selectedFlexComponents.push({
            id: $(this).data("id"),
            columns: $(this).data("columns")
        });
        
    });
  
    if (selectedFlexComponents.length > 0)
        $('#flexibleComponentsHidden').val(JSON.stringify(selectedFlexComponents));

    console.log(JSON.stringify(selectedFlexComponents));
};