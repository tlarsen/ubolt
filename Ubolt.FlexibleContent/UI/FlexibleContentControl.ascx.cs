﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using umbraco.IO;
using umbraco.controls.Tree;


namespace Ubolt.FlexibleContent.UI
{


    public partial class FlexibleContentControl : System.Web.UI.UserControl
    {
        public CustomTreeControl TreeControl;

        public object Value
        {
            get { return flexibleComponentsHidden.Value; } 
            set { flexibleComponentsHidden.Value = value.ToString(); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            TreeContainer.Controls.Add(TreeControl);
        }
    }
}