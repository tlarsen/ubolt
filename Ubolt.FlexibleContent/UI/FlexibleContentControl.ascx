﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FlexibleContentControl.ascx.cs" Inherits="Ubolt.FlexibleContent.UI.FlexibleContentControl" %>
<script src="/umbraco/plugins/Ubolt/FlexibleContent/FlexibleContentControl.js"></script>
<style type="text/css">
/*
	Variable Grid System.
	Learn more ~ http://www.spry-soft.com/grids/
	Based on 960 Grid System - http://960.gs/

	Licensed under GPL and MIT.
*/

/*
  Forces backgrounds to span full width,
  even if there is horizontal scrolling.
  Increase this if your layout is wider.

  Note: IE6 works fine without this fix.
*/

body {
  min-width: 600px;
}

/* Containers
----------------------------------------------------------------------------------------------------*/
.container_12 {
	margin-left: auto;
	margin-right: auto;
	width: 600px;
}

/* Grid >> Global
----------------------------------------------------------------------------------------------------*/


.grid_1,
.grid_2,
.grid_3,
.grid_4,
.grid_5,
.grid_6,
.grid_7,
.grid_8,
.grid_9,
.grid_10,
.grid_11,
.grid_12 {
	display:inline;
	float: left;
	position: relative;
	margin-left: 3px;
	margin-right: 2px;
}



.push_1, .pull_1,
.push_2, .pull_2,
.push_3, .pull_3,
.push_4, .pull_4,
.push_5, .pull_5,
.push_6, .pull_6,
.push_7, .pull_7,
.push_8, .pull_8,
.push_9, .pull_9,
.push_10, .pull_10,
.push_11, .pull_11,
.push_12, .pull_12 {
	position:relative;
}


/* Grid >> Children (Alpha ~ First, Omega ~ Last)
----------------------------------------------------------------------------------------------------*/

.alpha {
	margin-left: 0;
}

.omega {
	margin-right: 0;
}

/* Grid >> 12 Columns
----------------------------------------------------------------------------------------------------*/


.container_12 .grid_1 {
	width:45px;
}

.container_12 .grid_2 {
	width:95px;
}

.container_12 .grid_3 {
	width:145px;
}

.container_12 .grid_4 {
	width:195px;
}

.container_12 .grid_5 {
	width:245px;
}

.container_12 .grid_6 {
	width:295px;
}

.container_12 .grid_7 {
	width:345px;
}

.container_12 .grid_8 {
	width:395px;
}

.container_12 .grid_9 {
	width:445px;
}

.container_12 .grid_10 {
	width:495px;
}

.container_12 .grid_11 {
	width:545px;
}

.container_12 .grid_12 {
	width:595px;
}




/* Prefix Extra Space >> 12 Columns
----------------------------------------------------------------------------------------------------*/


.container_12 .prefix_1 {
	padding-left:50px;
}

.container_12 .prefix_2 {
	padding-left:100px;
}

.container_12 .prefix_3 {
	padding-left:150px;
}

.container_12 .prefix_4 {
	padding-left:200px;
}

.container_12 .prefix_5 {
	padding-left:250px;
}

.container_12 .prefix_6 {
	padding-left:300px;
}

.container_12 .prefix_7 {
	padding-left:350px;
}

.container_12 .prefix_8 {
	padding-left:400px;
}

.container_12 .prefix_9 {
	padding-left:450px;
}

.container_12 .prefix_10 {
	padding-left:500px;
}

.container_12 .prefix_11 {
	padding-left:550px;
}



/* Suffix Extra Space >> 12 Columns
----------------------------------------------------------------------------------------------------*/


.container_12 .suffix_1 {
	padding-right:50px;
}

.container_12 .suffix_2 {
	padding-right:100px;
}

.container_12 .suffix_3 {
	padding-right:150px;
}

.container_12 .suffix_4 {
	padding-right:200px;
}

.container_12 .suffix_5 {
	padding-right:250px;
}

.container_12 .suffix_6 {
	padding-right:300px;
}

.container_12 .suffix_7 {
	padding-right:350px;
}

.container_12 .suffix_8 {
	padding-right:400px;
}

.container_12 .suffix_9 {
	padding-right:450px;
}

.container_12 .suffix_10 {
	padding-right:500px;
}

.container_12 .suffix_11 {
	padding-right:550px;
}



/* Push Space >> 12 Columns
----------------------------------------------------------------------------------------------------*/


.container_12 .push_1 {
	left:50px;
}

.container_12 .push_2 {
	left:100px;
}

.container_12 .push_3 {
	left:150px;
}

.container_12 .push_4 {
	left:200px;
}

.container_12 .push_5 {
	left:250px;
}

.container_12 .push_6 {
	left:300px;
}

.container_12 .push_7 {
	left:350px;
}

.container_12 .push_8 {
	left:400px;
}

.container_12 .push_9 {
	left:450px;
}

.container_12 .push_10 {
	left:500px;
}

.container_12 .push_11 {
	left:550px;
}



/* Pull Space >> 12 Columns
----------------------------------------------------------------------------------------------------*/


.container_12 .pull_1 {
	left:-50px;
}

.container_12 .pull_2 {
	left:-100px;
}

.container_12 .pull_3 {
	left:-150px;
}

.container_12 .pull_4 {
	left:-200px;
}

.container_12 .pull_5 {
	left:-250px;
}

.container_12 .pull_6 {
	left:-300px;
}

.container_12 .pull_7 {
	left:-350px;
}

.container_12 .pull_8 {
	left:-400px;
}

.container_12 .pull_9 {
	left:-450px;
}

.container_12 .pull_10 {
	left:-500px;
}

.container_12 .pull_11 {
	left:-550px;
}




/* `Clear Floated Elements
----------------------------------------------------------------------------------------------------*/

/* http://sonspring.com/journal/clearing-floats */

.clear {
  clear: both;
  display: block;
  overflow: hidden;
  visibility: hidden;
  width: 0;
  height: 0;
}

/* http://www.yuiblog.com/blog/2010/09/27/clearfix-reloaded-overflowhidden-demystified */

.clearfix:before,
.clearfix:after {
  content: '\0020';
  display: block;
  overflow: hidden;
  visibility: hidden;
  width: 0;
  height: 0;
}

.clearfix:after {
  clear: both;
}

/*
  The following zoom:1 rule is specifically for IE6 + IE7.
  Move to separate stylesheet if invalid CSS is a problem.
*/

.clearfix {
  zoom: 1;
}
</style>
<style type="text/css">
    .componentList {
        border: 1px solid #B4B4B4;
        height: 300px;
        overflow-y: scroll;
        z-index: 39;
        width: 620px;
        float: left;
        margin: 5px;
    }

    .customTree {
        border: 1px solid #B4B4B4;
        height: 300px;
        overflow-y: scroll;
        z-index: 39;
        width: 275px;
        float: left;
        margin: 5px;
    }

    .component {
        height: 50px;
        background: rgb(238,238,238); /* Old browsers */
        background: -moz-linear-gradient(top, rgba(238,238,238,1) 0%, rgba(204,204,204,1) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(238,238,238,1)), color-stop(100%,rgba(204,204,204,1))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top, rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* IE10+ */
        background: linear-gradient(top, rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=0 ); /* IE6-9 */
    }

    .selected {
        height: 50px;
        background: rgb(255,197,120); /* Old browsers */
        background: -moz-linear-gradient(top, rgba(255,197,120,1) 0%, rgba(251,157,35,1) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,197,120,1)), color-stop(100%,rgba(251,157,35,1))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top, rgba(255,197,120,1) 0%,rgba(251,157,35,1) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top, rgba(255,197,120,1) 0%,rgba(251,157,35,1) 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top, rgba(255,197,120,1) 0%,rgba(251,157,35,1) 100%); /* IE10+ */
        background: linear-gradient(top, rgba(255,197,120,1) 0%,rgba(251,157,35,1) 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffc578', endColorstr='#fb9d23',GradientType=0 ); /* IE6-9 */
    }
    .sliderContainer
    {
        clear: both;
        padding-left: 340px;
        padding-top: 6px;
    }
   
</style>


<div class="propertypane">

    <div class="propertyItem">
        <div class="propertyItemContent">
            <div class="flexibleContentControl">
                <div class="customTree">
                    <asp:PlaceHolder runat="server" ID="TreeContainer"></asp:PlaceHolder>
                </div>
                <div class="componentList clearfix">
                    <!-- Insert some code to add optional grid system to be used -->
                    <div id="componentContainer" class="container container_12"></div>
                </div>
                <br/>
                <div class="sliderContainer">
                    <div class="slider"></div>
                    <div class="sliderinfo"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="propertyPaneFooter">-</div>
    <asp:HiddenField ID="flexibleComponentsHidden" ClientIDMode="Static" runat="server" />
</div>
