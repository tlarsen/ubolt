﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using Newtonsoft.Json;

namespace Ubolt.FlexibleContent
{
    public class FlexibleContentSaveData
    {
        public string Xml(string value)
        {
            var components = JsonConvert.DeserializeObject<List<dynamic>>(value);
            
            XmlDocument doc = createBaseXmlDocument();
            XmlNode root = doc.DocumentElement;

            foreach (dynamic component in components)
            {
                XmlNode componentNode = doc.CreateElement("Component");

                XmlNode NodeIdAttribute = doc.CreateNode(XmlNodeType.Attribute, "NodeId", null);
                NodeIdAttribute.Value = component.id;

                XmlNode ColumnsAttribute = doc.CreateNode(XmlNodeType.Attribute, "Columns", null);
                ColumnsAttribute.Value = component.columns;

                componentNode.Attributes.SetNamedItem(NodeIdAttribute);
                componentNode.Attributes.SetNamedItem(ColumnsAttribute);

                root.AppendChild(componentNode);
            }

            

            return doc.InnerXml;
        }

        private static XmlDocument createBaseXmlDocument()
        {
            XmlDocument doc = new XmlDocument();
            XmlNode root = doc.CreateElement("Components");
            doc.AppendChild(root);
            return doc;
        }

    }
}