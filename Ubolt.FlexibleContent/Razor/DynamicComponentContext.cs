﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.MacroEngines;
using umbraco.cms.businesslogic.macro;
using umbraco.interfaces;

namespace Ubolt.FlexibleContent.Razor
{
    public abstract class DynamicComponentContext : BaseContext<DynamicNode>
    {
        public override void SetMembers(MacroModel macro, INode node)
        {
            var page = PageData;
            var backingItem = new DynamicBackingItem(node);
            var dynamicNode = new DynamicNode(backingItem);
            CurrentModel = dynamicNode;
           
        }
    }
}