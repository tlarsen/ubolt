﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using Ubolt.FlexibleContent.Razor.Models;
using umbraco.MacroEngines;

namespace Ubolt.FlexibleContent.Razor
{
    [RazorDataTypeModel("b2ad320d-978c-436e-b5ee-02bcd41efe70")]
    public class FlexibleContentModelBinder : IRazorDataTypeModel 
    {
        public object PropertyData;
        public int CurrentNodeId;
        
        public bool Init(int CurrentNodeId, string PropertyData, out object instance)
        {
            this.CurrentNodeId = CurrentNodeId;
            this.PropertyData = PropertyData;
            var components = new Components();

            if(!string.IsNullOrEmpty(PropertyData))
            {

                var doc = new XmlDocument();
                doc.LoadXml(PropertyData);

                foreach (XmlNode node in doc.SelectNodes("//Component"))
                {
                    var component = new Component(node.SelectSingleNode("./@NodeId").InnerText,
                                                  node.SelectSingleNode("./@Columns").InnerText);

                    components.Items.Add(component);
                }

            }
                
            instance = components;
            return true;
        }
    }
}