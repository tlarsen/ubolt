﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using umbraco.MacroEngines.Library;
using umbraco.NodeFactory;

namespace Ubolt.FlexibleContent.Razor.Models
{
    public class Component
    {
        private Node _componentNode;
        public int NodeId { get; set; }
        public int Columns { get; set; }

        private string _useRendering;
        public string UseRendering
        {
            get { return _componentNode.GetProperty("useRendering").Value; }
            private set
            {
                _useRendering = value;
            }
        }

        public int MaxColumns { get; set; }
        public int MinColumns { get; set; }

        public Component(string nodeId, string columns)
        {
            NodeId = int.Parse(nodeId);
            Columns = int.Parse(columns);
            _componentNode = new Node(NodeId);
        }

        public HtmlString Rendered()
        {

            Node n = new Node(NodeId);
            var lib = new RazorLibraryCore(null);
            
            return new HtmlString(n.NodeTypeAlias);
        }

    }
}