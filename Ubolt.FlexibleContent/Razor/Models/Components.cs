﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Ubolt.FlexibleContent.Razor.Models
{
    public class Components : IEnumerable 
    {
        public List<Component> Items;

        public Components()
        {
            Items = new List<Component>(); ;
        }


        public IEnumerator GetEnumerator()
        {
            return Items.GetEnumerator();
        }

        public HtmlString Rendered()
        {

            //using (MemoryStream ms = new MemoryStream())
            //{
            //    using (TextWriter tw = new StreamWriter(ms))
            //    {
            //        using (HtmlTextWriter writer = new HtmlTextWriter(tw))
            //        {
            //            writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClassName);
            //            writer.RenderBeginTag(HtmlTextWriterTag.Ul);
            //        }
            //    }
            //}

            string val = String.Empty;

            foreach (var component in Items)
            {
                
                val += "<h1>" + component.NodeId + "</h1>";
                
            }
            
            return new HtmlString(val);
            
        }
    }
}