﻿using System;
using umbraco.cms.businesslogic.datatype;
using umbraco.editorControls;
using umbraco.interfaces;

namespace Ubolt.FlexibleContent
{
    public class FlexibleContentDataType : AbstractDataEditor
    {
        private FlexibleContentEditor _editor;
        private IData _data;
        private FlexibleContentPrevalueEditor _prevalueEditor;

        public FlexibleContentDataType()
        {
            
        }

        public override Guid Id
        {
            get { return new Guid("{b2ad320d-978c-436e-b5ee-02bcd41efe70}"); }
        }
        public override string DataTypeName
        {
            get { return "Flexible Content Editor"; }
        }

        public override IDataPrevalue PrevalueEditor
        {
            get { return _prevalueEditor ?? (_prevalueEditor = new FlexibleContentPrevalueEditor(this)); }
        }

        public override IDataEditor DataEditor
        {
            get { return _editor ?? (_editor = new FlexibleContentEditor(Data)); }
        }

        public override IData Data
        {
            get
            {
                if(_data == null)
                {
                    _data = new XmlData(this);
                }

                return _data;

            }
        }
    }
}