﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using ClientDependency.Core;
using umbraco.cms.presentation.Trees;
using umbraco.controls.Tree;
using umbraco.editorControls;
using umbraco.interfaces;
using umbraco.IO;



namespace Ubolt.FlexibleContent
{

    //[ClientDependency(ClientDependencyType.Javascript, "ui/jqueryui.js", "UmbracoClient")]
    //[ClientDependency(ClientDependencyType.Javascript, "/umbraco/plugins/FlexibleContent/FlexibleContentControl.js", "UmbracoClient")] 

    public class FlexibleContentEditor : PlaceHolder, IDataEditor
    {

        private readonly string _usercontrolPath = IOHelper.ResolveUrl(SystemDirectories.Umbraco) + "/plugins/Ubolt/FlexibleContent/FlexibleContentControl.ascx";
        private IData _data;

        private CustomTreeControl _treePickerControl;

        public Control Editor
        {
            get { return this; }
        }

        public void Save()
        {
            var saveData = new FlexibleContentSaveData();
            var c = (UI.FlexibleContentControl) Controls[0];
            _data.Value = saveData.Xml(c.Value.ToString());
        }

        public bool ShowLabel
        {
            get { return false; }
        }

        public bool TreatAsRichTextEditor
        {
            get { return true; }
        }

        public FlexibleContentEditor(IData data)
        {
            _data = data;
        }

        static FlexibleContentEditor()
        {
            if (!TreeDefinitionCollection.Instance.Any(x => x.TreeType == typeof(Tree.FlexibleContentTree)))
            {
                var contentTree = TreeDefinitionCollection.Instance.Single(x => x.Tree.Alias.ToUpper() == "CONTENT");
                var draggableContentTree = new TreeDefinition(typeof(Tree.FlexibleContentTree),
                    new umbraco.BusinessLogic.ApplicationTree(true, false, 0,
                        contentTree.Tree.ApplicationAlias,
                        "FlexibleContentTree",
                        contentTree.Tree.Title,
                        contentTree.Tree.IconClosed,
                        contentTree.Tree.IconOpened,
                        "Ubolt.FlexibleContent",
                        "Tree.FlexibleContentTree",
                        contentTree.Tree.Action),
                    contentTree.App);


                TreeDefinitionCollection.Instance.Add(draggableContentTree);
            }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            _treePickerControl = new CustomTreeControl
                                     {
                                         ID = "TreePicker",
                                         IsDialog = true,
                                         ShowContextMenu = false,
                                         DialogMode = TreeDialogModes.id,
                                         Height = Unit.Pixel(200),
                                         StartNodeID = 1057,
                                         TreeType = "FlexibleContentTree",
                                         App = "content"
                                     };

            var control = new UserControl().LoadControl(_usercontrolPath) as UI.FlexibleContentControl;
            
            if (control == null) 
                return;

            control.TreeControl = _treePickerControl;
            Controls.Add(control);
        }

        //protected override void Render(HtmlTextWriter writer)
        //{
        //    //<div class="multiTreePicker">
        //    //    <div class="header propertypane">
        //    //        <div>Select items</div>
        //    //    </div>
        //    //    <div class="left propertypane">        
        //    //        <umb:tree runat="server" ID="TreePickerControl" 
        //    //            CssClass="myTreePicker" Mode="Standard" 
        //    //            DialogMode="id" ShowContextMenu="false" 
        //    //            IsDialog="true" TreeType="content" />
        //    //    </div>
        //    //    <div class="right propertypane">
        //    //    </div>
        //    //</div>

            

        //    writer.AddAttribute("class", "Flex clearfix");
        //    writer.AddAttribute("id", this.ClientID);
        //    writer.RenderBeginTag(HtmlTextWriterTag.Div);

        //    writer.AddAttribute("class", "header propertypane");
        //    writer.RenderBeginTag(HtmlTextWriterTag.Div);
        //    writer.RenderBeginTag(HtmlTextWriterTag.Div);
        //    writer.Write("Select Items");
        //    writer.RenderEndTag();
        //    writer.RenderEndTag();

        //    writer.AddAttribute("class", "left propertypane");
        //    writer.AddStyleAttribute(HtmlTextWriterStyle.Height, ((200 + 10).ToString() + "px"));
        //    writer.RenderBeginTag(HtmlTextWriterTag.Div);
        //    //add the tree control here
        //    _treePickerControl.RenderControl(writer);
        //    writer.RenderEndTag();

        //    //RightColumn.RenderControl(writer);

        //    //render the hidden field
        //    //PickedValue.RenderControl(writer);

        //    writer.RenderEndTag(); //end multiNodePicker div

        //    var tooltipAjaxUrl = IOHelper.ResolveUrl(SystemDirectories.Umbraco) + @"/controls/Tree/CustomTreeService.asmx/GetNodeInfo";

        //    //add jquery window load event to create the js tree picker
        //    var jsMethod = string.Format("jQuery('#{0}').FlexTree('{1}', {2}, '{3}', {4}, {5}, '{6}', '{7}');",
        //        _treePickerControl.ClientID,
        //        this.ClientID,
        //        20,
        //        tooltipAjaxUrl,
        //        "false",
        //        "content",
        //        IOHelper.ResolveUrl(SystemDirectories.Umbraco),
        //        "content");
        //    var js = "jQuery(window).load(function() { " + jsMethod + " });";

        //    writer.WriteLine("<script type='text/javascript'>" + js + "</script>");

        //}

    }
}