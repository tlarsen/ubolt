﻿using System.Collections.Generic;
using umbraco;
using umbraco.BusinessLogic.Actions;
using umbraco.cms.businesslogic.web;
using umbraco.cms.presentation.Trees;
using umbraco.interfaces;

namespace Ubolt.FlexibleContent.Tree
{
    public class FlexibleContentTree : loadContent
    {
        public FlexibleContentTree(string app) : base(app)
        {
            
        }

        protected override void CreateAllowedActions(ref List<IAction> actions)
        {
            
            actions.Clear();
            actions.Add(ActionNew.Instance);
            
        }

        protected override void CreateRootNode(ref XmlTreeNode rootNode)
        {
            rootNode.Icon = FolderIcon;
            rootNode.OpenIcon = FolderIconOpen;
            rootNode.Text = "Content Items";
            rootNode.NodeType = "init" + TreeAlias;
            rootNode.NodeID = "init";
            rootNode.Action = "DoAlert('hest');";
        }

        protected override void OnRenderNode(ref XmlTreeNode xNode, Document doc)
        {
            base.OnRenderNode(ref xNode, doc);

            xNode.Action = "onNodeClick(" + xNode.NodeID + ");";
        }

    }
}

