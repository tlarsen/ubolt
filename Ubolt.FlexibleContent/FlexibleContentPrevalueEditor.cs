﻿using System.Web.UI;
using System.Web.UI.WebControls;
using umbraco.cms.businesslogic.datatype;
using umbraco.interfaces;

namespace Ubolt.FlexibleContent
{
    public class FlexibleContentPrevalueEditor : PlaceHolder, IDataPrevalue
    {
        private BaseDataType _dataType;

        public Control Editor
        {
            get { return this; }
        }

        public void Save()
        {
            _dataType.DBType = DBTypes.Nvarchar;

            PreValue preValue = PreValue.MakeNew(this._dataType.DataTypeDefinitionId, "test");
            preValue.Save();

        }


        public FlexibleContentPrevalueEditor(BaseDataType dataType)
        {
            _dataType = dataType;
        }
        
    }
}