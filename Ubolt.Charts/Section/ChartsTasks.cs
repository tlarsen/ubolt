﻿using System;
using System.Collections.Generic;
using Ubolt.Charts.Services;
using umbraco.BusinessLogic;
using umbraco.DataLayer;
using umbraco.DataLayer.SqlHelpers.SqlServer;
using umbraco.interfaces;
using umbraco.presentation.install.utills;

namespace Ubolt.Charts.Section
{
    public class ChartsTasks : ITaskReturnUrl
    {
        private string _alias;
        private int _parentID;
        private int _typeID;
        private int _userID;

        protected static ISqlHelper SqlHelper
        {
            get { return Application.SqlHelper; }
        }

        public int UserId
        {
            set { _userID = value; }
        }
        
        public int TypeID
        {
            set { _typeID = value; }
            get { return _typeID; }
        }
        
        public string Alias
        {
            set { _alias = value; }
            get { return _alias; }
        }

        public int ParentID
        {
            set { _parentID = value; }
            get { return _parentID; }
        }



        private string _returnUrl;
        public string ReturnUrl
        {
            get { return _returnUrl; }
        }

        

        public bool Delete()
        {
           return ChartDataService.Instance.DeleteChart(_parentID);
        }

        
        public bool Save()
        {
            //insert to db
            var chart = ChartDataService.Instance.CreateChart(_alias, _userID);

            _returnUrl = string.Format("plugins/Ubolt/charts/UI/Dashboard/editChart.aspx?id={0}", chart.Id);
            return true;

            //var chartId = SqlHelper.ExecuteScalar<int>("insert into uboltCharts (Alias, CreatedBy, Type, CreateDate, UpdateDate) values (@Alias, @CreatedBy, @Type, @CreateDate, @UpdateDate)", 
            //                                            new List<IParameter>()
            //                                            {
            //                                                SqlHelper.CreateParameter("@Alias", _alias),
            //                                                SqlHelper.CreateParameter("@Type", _typeID),
            //                                                SqlHelper.CreateParameter("@CreatedBy", _userID),
            //                                                SqlHelper.CreateParameter("@CreateDate", DateTime.Now),
            //                                                SqlHelper.CreateParameter("@UpdateDate", DateTime.Now)
            //                                            }.ToArray());

            
        }
        
    }
}