﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ubolt.Charts.Interfaces;
using Ubolt.Charts.Services;
using umbraco.cms.businesslogic.datatype;

namespace Ubolt.Charts
{
    public class ChartPickerDataType : AbstractDataEditor
    {
        private readonly ChartPicker _control = new ChartPicker();

        public override Guid Id
        {
            get
            {
                return new Guid("31DF182B-C44C-4F7B-AF99-998E2B0BAE3F");
            }
        }

        public override string DataTypeName
        {
            get
            {
                return "Ubolt Chart Picker";
            }
        }

        public ChartPickerDataType()
        {
            this.RenderControl = _control;
            _control.Init += _control_Init;
            base.DataEditorControl.OnSave += DataEditorControl_OnSave;
        }

        void _control_Init(object sender, EventArgs e)
        {
            if (base.Data.Value != null && base.Data.Value.ToString() != string.Empty)
            {
                var chart = ChartDataService.Instance.GetChart(base.Data.Value.ToString());
                _control.DataValue = chart.Alias;
            }
        }

       

        protected void DataEditorControl_OnSave(EventArgs e)
        {
            base.Data.Value = ((UI.ChartPickerControl)_control.Controls[0]).value.ToString();
        }


    }
}