﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.interfaces;
using umbraco.uicontrols.TreePicker;

namespace Ubolt.Charts
{
    public class ChartPickerMacroRender : ChartPicker, IMacroGuiRendering
    {

        private string _value = string.Empty;

        public bool ShowCaption
        {
            get { return false; }
        }

        public string Value
        {
            //get { return ((UI.ChartPickerControl)Controls[0]).value.ToString(); }
            get { return ((UI.ChartPickerControl)Controls[0]).value.ToString(); }
            set { _value = value; }
        }

    }
}