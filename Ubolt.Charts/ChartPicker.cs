﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ubolt.Charts.Interfaces;
using umbraco.IO;

namespace Ubolt.Charts
{
    public class ChartPicker : PlaceHolder
    {
        private string _path = IOHelper.ResolveUrl(SystemDirectories.Umbraco) + "/plugins/Ubolt/Charts/UI/ChartPickerControl.ascx";
        private object _dataValue;

        public object DataValue
        {
            get { return ((UI.ChartPickerControl)Controls[0]).value;  }
            set { _dataValue = value; }
        }


        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Controls.Add(new UserControl().LoadControl(_path));
        }

        protected override void OnLoad(EventArgs e)
        {
            ((UI.ChartPickerControl) Controls[0]).value = _dataValue;
        }
       
        
    }
}