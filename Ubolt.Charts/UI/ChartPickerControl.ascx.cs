﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ubolt.Charts.Interfaces;
using umbraco.DataLayer;
using umbraco.editorControls.userControlGrapper;

namespace Ubolt.Charts.UI
{
    public partial class ChartPickerControl : System.Web.UI.UserControl, IUsercontrolDataEditor
    {
        private object _value;
        public object value
        {
            get { return HiddenChartId.Value; }
            set
            {
                if (value != null)
                    _value = value.ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (_value != null)
                selectedChart.InnerText  = _value.ToString();

            
        }

    }
}