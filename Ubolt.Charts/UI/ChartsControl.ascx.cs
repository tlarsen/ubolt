﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ubolt.Charts.Interfaces;
using umbraco.editorControls.userControlGrapper;

namespace Ubolt.Charts.UI
{
    public partial class ChartsControl : System.Web.UI.UserControl, IUsercontrolDataEditor,  IChartsEditor
    {
        public bool EnableColumnChart { get; set; }
        public bool EnableBarChart { get; set; }
        public bool EnableLineChart { get; set; }
        public bool EnableCurveChart { get; set; }
        public bool EnablePieChart { get; set; }
        public bool EnableGridLines { get; set; }
        public bool EnableChartTitle { get; set; }
        public int ChartWidth { get; set; }
        public int ChartHeight { get; set; }

        public ControlCollection OptionControls { get; set; }

        public Guid UniqueChartId { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            UniqueChartId = Guid.NewGuid();
        }

        private string _umbracoValue;
        public object value
        {
            get
            {
                return javaScriptArrayHidden.Value;
            }
            set
            {
                if (value != null)
                    _umbracoValue = value.ToString();
            }
        }
    }
}