(function (window, $) {
    "use strict";
    var uChart, uChartDataHandler, uChartRenderHandler, uChartComponentHandler;

    window.uChart = uChart = (function () {

        function Chart(obj) {
            var key,
                value,
                defaults = {
                    settings: {
                        data: [

                        ],
                        options: {
                            title: "",
                            titleTextStyle: {
                                color: '#094167',
                                fontSize: 24
                            },
                            legend: {
                                position: "bottom"
                            },
                            hAxis: {
                                gridlines: {
                                    color: "#fff"
                                }
                            },
                            backgroundColor: '#ecf1f5',
                            vAxis: {
                                textStyle: {
                                    color: '#094167'
                                },
                                gridlines: {
                                    color: '#fff'
                                }
                            },
                            fontSize: 12,
                            animation: {
                                duration: 1000,
                                easing: "out"
                            },
                            chartArea: {
                                left: 50,
                                top: 50,
                                width: "80%",
                                height: "70%"
                            },
                            colors: [
                                { color: "#0a4167", visibleInLegend: false },
                                { color: "#47718d", visibleInLegend: false },
                                { color: "#666666", visibleInLegend: false },
                                { color: "#a39262", visibleInLegend: false },
                                { color: "#afae9a", visibleInLegend: false },
                                { color: "#dbdbd3", visibleInLegend: false }
                            ],

                        },
                        
                        chartType:"Column"
                    }
                };

            obj = $.extend(obj, defaults);

            for (key in obj) {
                value = obj[key];
                this[key] = value;
            }
        };
        return Chart;

    })();

    window.uChartDataHandler = uChartDataHandler = (function(){
        var updateOptions = function (keys, value, self) {

            if (keys == "hAxis.slantedTextAngle") {
                if (value == 0) {
                    self.settings.options.hAxis.slantedText = false;
                } else {
                    self.settings.options.hAxis.slantedText = true;
                }

            }

            
            $.each(keys, function () {
          
                if (!keys || !keys.length) {
                    return;
                }

                if (!/\./g.test(this)) {
                    self.settings.options[this] = value;
                }
                else {
                    var arrayKey = this.split(/\./g),
                        obj;

                    $.each(arrayKey, function (i) {
                        if (!i) {
                            obj = self.settings.options[this];
                            if (!obj) {
                                obj = self.settings.options[this] = {};
                            }
                            return true;
                        }
                        else if (i === arrayKey.length - 1) {
                            obj[this] = value;
                            if (!obj) {
                                obj = self.settings.options[this] = {};
                            }
                            return false;
                        }
                        if (!obj[this]) {
                            obj = obj[this] = {};
                            return true;
                        }
                        obj = obj[this];
                    });
                }
            });
        };

        //setting the values from all settings controls
        var setOptions = function (control, chart) {
            var $control = $(control), value;
            var key = $control.data("key");

            if (key === null || key === undefined) {
                return;
            }
            key = key.replace(/\s/g, "");
            var keys = key.split(/,/g);

            value = getValueFromElement.call(control);
            
            updateOptions(keys, value, chart);
            
            
            $(chart.hiddenSettingsId).val(JSON.stringify(chart.settings));
            uChartRenderHandler.updateChart(chart);
        };


        var getValueFromElement = function () {
            
            switch (this.type) {
                case "text":
                    if (!/[^,\.\d\s]+/g.test(this.value)) {
                        return parseFloat(this.value);
                    }
                    return (this.value);
                case "checkbox":
                    return this.checked;
                case "radio":
                    return this.checked;
                case "select-one":
                    return this.value;
                default:
                    return;
            }
        };

        var setData = function (chart) {
            var handsonTable = $(chart.handsonTableId).data("handsontable");

            chart.settings.data = formatChartData(handsonTable.getData(), chart);

            $(chart.hiddenSettingsId).val(JSON.stringify(chart.settings));
           
        };

        var formatChartData = function (array, chart) {
            $.each(array, function (i1) {
                if (i1 > 0) {
                    $.each(this, function (i2) {
                        if (i2 > 0 && i1 > 0) {
                            
                            //test to see if it is a org chart if so then parse data to string.
                            //if (chart.settings.chartType == "Org") {
                            //    array[i1][i2] = this;
                            //} else {
                            
                            array[i1][i2] = parseFloat(this.replace(',', '.'));
                            //}
                            
                        }
                    });
                }
                this.splice(this.length - 1, 1);
            });
            array.splice(array.length - 1, 1);

            return array;
        };

        return {
            setData: setData,
            setOptions: setOptions
        };

    })();

    window.uChartRenderHandler = uChartRenderHandler = (function () {
        var googleChart;
        google.load("visualization", "1", { packages: ["corechart", "orgchart"] });

        var redrawChart = function (chart) {
            render(chart, true);
        };

        var updateChart = function (chart) {
            render(chart, true);
        };

        var redrawOrUpdate = function (control, chart) {
          
            var key = $(control).data("key");
            
            if (key == "Area") {
                chart.settings.options.areaOpacity = "1.0";
            } else {
                chart.settings.options.areaOpacity = "1.0";
            }

            chart.settings.options.curveType = key === "Curve" ? "function" : "none";
            key = key === "Curve" ? "Line" : key;
            chart.settings.chartType = key;
            
            $(chart.hiddenSettingsId).val(JSON.stringify(chart.settings));
            render(chart, true);
            


        };

        var render = function (chart, doRedraw) {
            if (doRedraw || googleChart == undefined) {
                googleChart = new google.visualization[chart.settings.chartType + "Chart"]($(chart.googleChartDivId)[0]);
            }
            googleChart.draw(google.visualization.arrayToDataTable(chart.settings.data), chart.settings.options);
            
            //update base64 data on redraw
            var chartContainer = $(chart.googleChartDivId)[0];
            var base64Chart = convertChartToBase64(chartContainer);
            $(chart.hiddenBase64Id).val(base64Chart);
            console.log(base64Chart);

        };

        var convertChartToCanvas = function (chartContainer) {
            var grandchild = $(chartContainer).children(":first").children(":first");
            var chartArea = grandchild[0];

            var svg = chartArea.innerHTML;
            var doc = chartContainer.ownerDocument;
            var canvas = doc.createElement("canvas");
            canvas.setAttribute("width", chartArea.offsetWidth);
            canvas.setAttribute("height", chartArea.offsetHeight);

            canvas.setAttribute(
                "style",
                "position: absolute; " +
                    "top: " + (-chartArea.offsetHeight * 2) + "px;" +
                    "left: " + (-chartArea.offsetWidth * 2) + "px;");
            doc.body.appendChild(canvas);
            canvg(canvas, svg);
            return canvas;

        };

        var convertChartToBase64 = function (chartContainer) {

            var canvas = convertChartToCanvas(chartContainer);
            var base64Data = canvas.toDataURL("image/png");

            return base64Data;
        };


        return {
            redrawChart: redrawChart,
            updateChart: updateChart,
            redrawOrUpdate: redrawOrUpdate
        };
    })();

    window.uChartComponentHandler = uChartComponentHandler = (function(){

        var initHandsonTable = function(chart){
            var $dataTable = $(chart.handsonTableId);
            
            $dataTable.handsontable({
                rows: 1,
                cols: 1,
                minSpareCols: 1,
                minSpareRows: 1,
                onChange: function (data, source) {
                    if (source === 'loadData') {
                        return;
                    }

                    uChartDataHandler.setData(chart);
                    uChartRenderHandler.updateChart(chart);

                }
            });
            
            $dataTable.handsontable("loadData", chart.settings.data);
        };

        var initChartChooser = function(chart){

            $(chart.chartTypeClass).each(function () {
                var $this = $(this),
                    key = $this.data("key");

                if (key === null || key === undefined) {
                    return true;
                }

                if (chart.settings.chartType === null || chart.settings.chartType === undefined) {
                    return true;
                }

                if (key == chart.settings.chartType) {
                    $this.attr('checked', true);
                }

                uChartRenderHandler.redrawChart(chart);
            });
        };

        var initChartSettings = function(chart){
            $(chart.chartSettingsClass).each(function () {
                var $this = $(this),
                    key = $this.data("key");

                if (key === null || key === undefined) {
                    return true;
                }

                key = key.replace(/\s/g, "").split(/,/g)[0];

                if (!/\./g.test(key)) {
                    $this.val(chart.settings.options[key]);
                }
                else {
                    var arrayKey = key.split(/\./g),
                        obj = chart.settings.options;

                    $.each(arrayKey, function () {
                        if (obj) {
                            obj = obj[this];
                        }
                    });

                    if (obj) {
                        $this.val(obj);
                    }
                }
            });
        };

        var submitChart = function (chart) {
            
        };
        

        var initUCharts = function (chart) {
           
            //bind to the save event
            $(chart.saveButton).click(function() {
                submitChart(chart);
            });

            //determin if chart has data from the database and sets settings corrosponding.    
            if ($(chart.hiddenSettingsId).val() != null && $(chart.hiddenSettingsId).val() != "") {
                chart.settings = $.parseJSON($(chart.hiddenSettingsId).val());
            } else {
                $(chart.hiddenSettingsId).val(JSON.stringify(chart.settings));
            }


            $(window).on("change", chart.chartSettingsClass, function(){uChartDataHandler.setOptions(this, chart)});
            $(window).on("change", chart.chartTypeClass, function(){ uChartRenderHandler.redrawOrUpdate(this, chart)});

            initHandsonTable(chart);
            initChartChooser(chart);
            initChartSettings(chart);
        };

        return {
            initUCharts: initUCharts
        };

    })();



}).call(this, window, jQuery);