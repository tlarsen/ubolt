﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="/Umbraco/masterpages/umbracoPage.Master" Title="Charts" CodeBehind="showChart.aspx.cs" Inherits="Ubolt.Charts.UI.Dashboard.showChart" %>

<%@ Register TagPrefix="umb" Namespace="umbraco.uicontrols" Assembly="controls" %>
<%@ Register TagPrefix="client" Namespace="ClientDependency.Core.Controls" Assembly="ClientDependency.Core" %>


<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <umb:UmbracoPanel ID="UmbracoPanel1" runat="server" Width="496px" Height="584px">
        <umb:Pane ID="Pane1" runat="server">

            <umb:PropertyPanel runat="server" Text="chart liste">
                Her er der charts:
            </umb:PropertyPanel>

        </umb:Pane>
    </umb:UmbracoPanel>
</asp:Content>
