﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ubolt.Charts.Models;
using Ubolt.Charts.Services;
using umbraco.BasePages;
using umbraco.DataLayer;

namespace Ubolt.Charts.UI.Dashboard
{
    public partial class editChart : UmbracoEnsuredPage
    {
        public ImageButton saveButton;
        public ImageButton resetButton;

        public string UniqueChartId { get; set; }
        
        public static ISqlHelper SqlHelper
        {
            get { return umbraco.BusinessLogic.Application.SqlHelper; }
        }

        protected override void OnInit(EventArgs e)
        {
            saveButton = umbPanel.Menu.NewImageButton();
            saveButton.ImageUrl = string.Format("{0}/images/editor/save.gif", umbraco.GlobalSettings.Path);
            saveButton.ToolTip = "Save chart data";
            saveButton.Click += btnSave_Click;
            saveButton.ID = "save";

            resetButton = umbPanel.Menu.NewImageButton();
            resetButton.ImageUrl = string.Format("{0}/images/editor/upload.png", umbraco.GlobalSettings.Path);
            resetButton.ToolTip = "Reset chart data";
            resetButton.Click += btnReset_Click;
            //ImageButton btnHelp = umbPanel.Menu.NewImageButton();
            //btnHelp.ImageUrl = string.Format("{0}/images/editor/help.png", umbraco.GlobalSettings.Path); 



            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UniqueChartId = Guid.NewGuid().ToString();
                PopulateChartSettings(Convert.ToInt32(Request.QueryString["id"]));
            }
        }

        void btnReset_Click(object sender, ImageClickEventArgs e)
        {
            ResetChart(Convert.ToInt32(Request.QueryString["id"]));
        }

        void btnSave_Click(object sender, ImageClickEventArgs e)
        {
            SaveChart(Convert.ToInt32(Request.QueryString["id"]));
        }

        protected void ResetChart(int chartId)
        {
            var chart = new ChartModel();

            chart.Id = chartId;
            chart.Alias = tbChartTitle.Text;
            chart.UpdateDate = DateTime.Now;
            chart.Type = 1; //not used yet. should be used for icon selection.
            ChartDataService.Instance.ResetChart(chart);

            ClientTools.ShowSpeechBubble(speechBubbleIcon.info, "Chart reset", "Any errors are gone now.");
            
        }

        protected void PopulateChartSettings(int chartId)
        {
            var chart = ChartDataService.Instance.GetChart(chartId);
            
            //updateing preview values.
            chart_title.InnerText = chart.Alias;
            chart_description.InnerText = chart.Source;

            //updating textboxes.
            tbChartTitle.Text = chart.Alias;
            tbHorisontalTitle.Text = chart.HoristontalAxis;
            tbVerticalTitle.Text = chart.VerticalAxis;
            tbChartSource.Text = chart.Source;
            tbChartDescription.Text = chart.Description;
            tbChartNotes.Text = chart.Notes;
            
            //setting data that gets send to google. and used by the editor to populate fields.
            javaScriptArrayHidden.Value = chart.Data;
            
        }

        protected void SaveChart(int chartId)
        {
            var chart = new ChartModel();
            var t = Base64ChartHidden.Value;
            chart.Id = chartId;
            chart.Alias = tbChartTitle.Text;
            chart.HoristontalAxis = tbHorisontalTitle.Text;
            chart.VerticalAxis = tbVerticalTitle.Text;
            chart.Data = javaScriptArrayHidden.Value;
            chart.Description = tbChartDescription.Text;
            chart.StaticImagePath = ChartImageService.Instance.CreateImage(Base64ChartHidden.Value, chartId);
            chart.Notes = tbChartNotes.Text;
            chart.Source = tbChartSource.Text;
            chart.UpdateDate = DateTime.Now;
            chart.Type = 1; //note used yet. should be used for icon selection.

            ChartDataService.Instance.UpdateChart(chart);

           ClientTools.ShowSpeechBubble(speechBubbleIcon.save, "Chart saved", "And ready to be used on content pages.");
        }
        
    }
}