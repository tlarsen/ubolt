﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChartPickerControl.ascx.cs" Inherits="Ubolt.Charts.UI.ChartPickerControl" %>
<script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
<style>
    .ui-helper-hidden-accessible {
        display: none;
    }

    #searchChart {
        width: 25em;
    }
</style>

<script>
    $(function () {
        function setActiveChart(chart) {
            $("#selectedChart").html($("<div>").text(chart.label));
            $("#HiddenChartData").val(chart.data);
            $("#HiddenChartId").val(chart.id);
        }

        function setChartSize(value) {
            $("#HiddenChartCssClass").val(value);
        }

        $("input:radio[name=size]").click(function () {
            setChartSize($(this).val());
        });

        $("#searchChart").autocomplete({
            source: function (request, response) {
                $.ajax({
                    contentType: "application/json; charset=utf-8",
                    type: "POST",
                    url: "/umbraco/plugins/ubolt/charts/services/visualizationpickerservice.asmx/GetCharts",
                    dataType: "json",
                    data: "{startsWith: '" + request.term + "'}",
                    success: function (data) {
                        data = jQuery.parseJSON(data.d);
                        response($.map(data, function (item) {
                            return {
                                label: item.Name,
                                data: item.Data,
                                id: item.ID
                            };
                        }));
                    }
                });
            },
            minLength: 2,
            select: function (event, chart) {
                setActiveChart(chart.item);
            }
        });
    });
</script>

<div>
    <label for="searchChart">Vælg et chart</label>
    <input id="searchChart" placeholder="Skriv navnet på det chart du vil indsætte" />
    <div>
        <label for="cssClass-wide"><input type="radio" id="cssClass-wide" name="cssClass" value="wide" />Bred</label>
        <label for="cssClass-small"><input type="radio" id="cssClass-small" name="cssClass" value="narrow" />Small</label>
        <label for="cssClass-badge"><input type="radio" id="cssClass-badge" name="cssClass" value="badge" />Badge</label>
    </div>
</div>
<div style="margin-top: 20px;">
    Du har valgt :
    <div id="selectedChart" runat="server" clientidmode="Static"></div>
</div>


<asp:HiddenField ID="HiddenChartId" runat="server" ClientIDMode="Static" />
<asp:HiddenField ID="HiddenChartData" runat="server" ClientIDMode="Static" />
<asp:HiddenField ID="HiddenChartCssClass" runat="server" ClientIDMode="Static" />
