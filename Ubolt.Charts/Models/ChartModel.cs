﻿using System;
using System.Collections.Generic;


namespace Ubolt.Charts.Models
{
    public class ChartModel
    {
        public int Id { get; set; }

        public string Alias { get; set; }
        
        public string Description { get; set; }
        
        public string Data { get; set; }
        
        public string Source { get; set; }
        
        public string Notes { get; set; }

        public string StaticImagePath { get; set; }
        
        public int Type { get; set; }
        
        public string VerticalAxis { get; set; }
        
        public string HoristontalAxis { get; set; }
        
        public DateTime UpdateDate { get; set; }
        
        public DateTime CreateDate { get; set; }
        
        public int CreatedBy { get; set; }  
    }
}