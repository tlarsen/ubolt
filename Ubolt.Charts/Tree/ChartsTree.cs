﻿using System;
using Ubolt.Charts.Services;
using umbraco.BusinessLogic;
using umbraco.DataLayer;
using umbraco.businesslogic;
using umbraco.cms.presentation.Trees;

namespace Ubolt.Charts.Tree
{
    [Tree("chartsApp", "chartsTree", "Google charts", sortOrder: 1)]
    public class ChartsTree : BaseTree
    {

        public ChartsTree(string application) : base(application)
        {
            
        }
        protected static ISqlHelper SqlHelper
        {
            get { return Application.SqlHelper; }
        }
        protected override void CreateRootNode(ref XmlTreeNode rootNode)
        {
            rootNode.Icon = "chart_pie.png";
            rootNode.OpenIcon = "chart_pie.png";
            rootNode.NodeType = TreeAlias;
            rootNode.NodeID = "init";
        }

        public override void Render(ref XmlTree tree)
        {
            var charts = ChartDataService.Instance.GetCharts(fast: true);
            
            foreach (var chart in charts)
            {
                XmlTreeNode node = XmlTreeNode.Create(this);
                node.NodeID = chart.Id.ToString();
                node.Text = chart.Alias;
                node.Action = "javascript:openChart(" + chart.Id + ");";
                
                //TODO: Create IconResolver that can return the right icon based on the chart type.
                node.Icon = "chart_bar.png"; 
                node.OpenIcon = "chart_bar.png";
                OnBeforeNodeRender(ref tree, ref node, EventArgs.Empty);
                if (node != null)
                {
                    tree.Add(node);
                }
                OnAfterNodeRender(ref tree, ref node, EventArgs.Empty);
            }
        }




        public override void RenderJS(ref System.Text.StringBuilder Javascript)
        {
            Javascript.Append(
                @"
                    function openChart(id) {
                        UmbClientMgr.contentFrame('plugins/Ubolt/charts/UI/Dashboard/editChart.aspx?id=' + id);
                    } 

                    function showChart(id) {
                        UmbClientMgr.contentFrame('plugins/Ubolt/charts/UI/Dashboard/showChart.aspx?id=' + id);
                    } 
                ");

        }
    }
}
