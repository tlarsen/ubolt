﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Ubolt.Charts.Models;
using umbraco.DataLayer;

namespace Ubolt.Charts.Services
{
    internal class ChartDataService
    {
        private static ChartDataService _instance = null;

        public static ChartDataService Instance
        {
            get { return _instance ?? (_instance = new ChartDataService()); }
        }

        internal ChartDataService()
        {

        }


        private static ISqlHelper SqlHelper
        {
            get { return umbraco.BusinessLogic.Application.SqlHelper; }
        }

        internal IEnumerable<ChartModel> GetCharts(bool fast = false)
        {
            var list = new List<ChartModel>();
            if (!fast)
            {
                using (IRecordsReader charts = SqlHelper.ExecuteReader("select * from uboltCharts"))
                {
                    while (charts.Read())
                    {
                        list.Add(new ChartModel()
                        {
                            Id = charts.GetInt("id"),
                            Alias = charts.GetString("Alias"),
                            Data = charts.GetString("Data"),
                            Source = charts.GetString("Source"),
                            Description = charts.GetString("Description"),
                            StaticImagePath = charts.GetString("StaticImagePath"),
                            Notes = charts.GetString("Notes"),
                            Type = charts.GetInt("Type"),
                            HoristontalAxis = charts.GetString("HAxisTitle"),
                            VerticalAxis = charts.GetString("VAxisTitle"),
                            CreateDate = charts.GetDateTime("CreateDate"),
                            UpdateDate = charts.GetDateTime("UpdateDate"),
                            CreatedBy = charts.GetInt("CreatedBy")
                        });
                    }
                }
               
                
            }
            else
            {
                using (IRecordsReader charts = SqlHelper.ExecuteReader("select id, Alias, Type from uboltCharts"))
                {
                    while (charts.Read())
                    {
                        list.Add(new ChartModel()
                            {
                                Id = charts.GetInt("id"),
                                Alias = charts.GetString("Alias"),
                                Type = charts.GetInt("Type")
                            });
                    }
                }
            }

            return list;
        }

        internal IEnumerable<ChartModel> GetCharts(FilterModel filter)
        {
            var list = new List<ChartModel>();
            var sql = new StringBuilder();
            var parameters = new List<IParameter>();

            sql.Append("select * from uboltCharts ");

            if (!String.IsNullOrEmpty(filter.StartsWith))
            {
                sql.Append("where Alias LIKE @StartsWith");
                parameters.Add(SqlHelper.CreateParameter("@StartsWith", "%" + filter.StartsWith + "%"));
            }

            using (IRecordsReader charts = SqlHelper.ExecuteReader(sql.ToString(), parameters.ToArray()))
            {
                while (charts.Read())
                {
                    list.Add(new ChartModel()
                        {
                            Id = charts.GetInt("id"),
                            Alias = charts.GetString("Alias"),
                            Data = charts.GetString("Data"),
                            Source = charts.GetString("Source"),
                            Type = charts.GetInt("Type"),
                            StaticImagePath = charts.GetString("StaticImagePath"),
                            HoristontalAxis = charts.GetString("HAxisTitle"),
                            VerticalAxis = charts.GetString("VAxisTitle"),
                            CreateDate = charts.GetDateTime("CreateDate"),
                            UpdateDate = charts.GetDateTime("UpdateDate"),
                            CreatedBy = charts.GetInt("CreatedBy")
                        });
                }
            }

            return list;
        }

        internal ChartModel GetChart(int id)
        {
            return GetChart(id.ToString());
        }

        internal ChartModel GetChart(string id)
        {
            var model = new ChartModel();

            using (IRecordsReader chart = SqlHelper.ExecuteReader("select * from uboltCharts where id = @id", SqlHelper.CreateParameter("@id", id)))
            {
                if (chart.Read())
                {
                    model.Id = chart.GetInt("id");
                    model.Alias = chart.GetString("Alias");
                    model.Data = chart.GetString("Data");
                    model.Source = chart.GetString("Source");
                    model.Description = chart.GetString("Description");
                    model.StaticImagePath = chart.GetString("StaticImagePath");
                    model.Notes = chart.GetString("Notes");
                    model.Type = chart.GetInt("Type");
                    model.HoristontalAxis = chart.GetString("HAxisTitle");
                    model.VerticalAxis = chart.GetString("VAxisTitle");
                    model.CreateDate = chart.GetDateTime("CreateDate");
                    model.UpdateDate = chart.GetDateTime("UpdateDate");
                    model.CreatedBy = chart.GetInt("CreatedBy");
                }
            }
            return model;
        }


        internal ChartModel CreateChart(string alias, int userId)
        {
            var parameters = new List<IParameter>()
                {
                    SqlHelper.CreateParameter("@Alias", alias),
                    SqlHelper.CreateParameter("@CreateDate", DateTime.Now),
                    SqlHelper.CreateParameter("@UpdateDate", DateTime.Now),
                    SqlHelper.CreateParameter("@CreatedBy", userId),
                };

            int chartId = SqlHelper.ExecuteScalar<int>("insert into uboltCharts (Alias, CreateDate, UpdateDate, CreatedBy) OUTPUT Inserted.id  values (@Alias, @CreateDate, @UpdateDate, @CreatedBy)", parameters.ToArray());

            return GetChart(chartId);

        }

        internal void UpdateChart(ChartModel model)
        {
            var parameters = new List<IParameter>()
                {
                    SqlHelper.CreateParameter("@id", model.Id),
                    SqlHelper.CreateParameter("@Alias", model.Alias),
                    SqlHelper.CreateParameter("@HAxisTitle", model.HoristontalAxis),
                    SqlHelper.CreateParameter("@VAxisTitle", model.VerticalAxis),
                    SqlHelper.CreateParameter("@Type", model.Type),
                    SqlHelper.CreateParameter("@Data", model.Data),
                    SqlHelper.CreateParameter("@Source", model.Source),
                    SqlHelper.CreateParameter("@Description", model.Description),
                    SqlHelper.CreateParameter("@StaticImagePath", model.StaticImagePath),
                    SqlHelper.CreateParameter("@Notes", model.Notes),
                    SqlHelper.CreateParameter("@UpdateDate", DateTime.Now),
                };
            SqlHelper.ExecuteNonQuery("update uboltCharts set Alias = @Alias, HAxisTitle = @HAxisTitle, VAxisTitle = @VAxisTitle, Type = @Type, Data = @Data, Source = @Source, Description = @Description, StaticImagePath = @StaticImagePath, Notes = @Notes, UpdateDate = @UpdateDate where id = @id", parameters.ToArray());

        }



        internal bool DeleteChart(int id)
        {
            return SqlHelper.ExecuteNonQuery("delete from uboltCharts where id = @id", SqlHelper.CreateParameter("@id", id)) > 0 ? true : false;
        }

        internal void ResetChart(ChartModel model)
        {
            model.Data = string.Empty;
            model.Description = string.Empty;
            model.HoristontalAxis = string.Empty;
            model.VerticalAxis = string.Empty;
            model.Notes = string.Empty;
            model.Source = string.Empty;
            model.StaticImagePath = string.Empty;
            UpdateChart(model);
        }
    }
}