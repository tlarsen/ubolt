﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using Newtonsoft.Json;
using Ubolt.Charts.Models;
using umbraco.DataLayer;
using System.Security.Cryptography;

namespace Ubolt.Charts.Services
{
    /// <summary>
    /// Summary description for ChartPickerService
    /// </summary>
    [WebService(Namespace = "http://ubolt.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class ChartPickerService : System.Web.Services.WebService
    {
        /// <summary>
        /// Used by auto complete 
        /// </summary>
        /// <param name="startsWith"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetCharts(string startsWith)
        {
            var filter = new FilterModel
                {
                    StartsWith = startsWith
                };

            var charts = ChartDataService.Instance.GetCharts(filter);

            return JsonConvert.SerializeObject(charts);
        }

        [WebMethod]
        public string GetChartData(string id)
        {
            var chart = ChartDataService.Instance.GetChart(id);
            if (chart == null)
                return string.Empty;

            return JsonConvert.SerializeObject(chart);
        }
        
    }
}
