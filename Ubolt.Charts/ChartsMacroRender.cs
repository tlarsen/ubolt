﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using umbraco.interfaces;

namespace Ubolt.Charts
{
    //[DefaultProperty("Text"), ToolboxData("<{0}:ChartsMacroRender runat=server Version=\"\" Alias=\"\"></{0}:ChartsMacroRender>")]
    public class ChartsMacroRender : ChartsEditor, IMacroGuiRendering
    {
        private string _value = string.Empty;

        public bool ShowCaption
        {
            get { return true; }
        }
        
        //[Bindable(true), Category("Umbraco"), DefaultValue(""), Browsable(true)]
        public string Value
        {
            get { return ((UI.ChartsControl)Controls[0]).value.ToString(); }
            set { _value = value; }
        }

        public ChartsMacroRender()
        {

        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.EnableBarChart =
            this.EnableChartTitle =
            this.EnableColumnChart =
            this.EnableCurveChart =
            this.EnableGridLines =
            this.EnableLineChart =
            this.EnablePieChart =
            true;

            this.OptionControls = new ControlCollection(this);

            if (this.FirstLoad)
            {
                ((UI.ChartsControl)Controls[0]).value = _value;
                FirstLoad = false;
            }


        }
    }
}