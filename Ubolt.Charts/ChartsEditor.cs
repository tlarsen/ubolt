﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ubolt.Charts.Interfaces;
using umbraco.IO;

namespace Ubolt.Charts
{
    public class ChartsEditor : PlaceHolder, IChartsEditor
    {
        private string _path = IOHelper.ResolveUrl(SystemDirectories.Umbraco) + "/plugins/Ubolt/Charts/ChartsControl.ascx";
        private object _value;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Controls.Add(new UserControl().LoadControl(_path));
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected bool FirstLoad
        {
            get
            {
                if (ViewState["FirstLoad"] == null)
                {
                    ViewState["FirstLoad"] = true;
                }

                return Convert.ToBoolean(ViewState["FirstLoad"]);
            }
            set
            {
                ViewState["FirstLoad"] = value;
            }
        }

        public object EditorValue
        {
            get
            {
                return ((UI.ChartsControl)Controls[0]).value;
            }
            set
            {
                _value = value;
            }
        }

        public bool EnableColumnChart { get; set; }
        public bool EnableBarChart { get; set; }
        public bool EnableLineChart { get; set; }
        public bool EnableCurveChart { get; set; }
        public bool EnablePieChart { get; set; }
        public bool EnableGridLines { get; set; }
        public bool EnableChartTitle { get; set; }
        public int ChartWidth { get; set; }
        public int ChartHeight { get; set; }

        public ControlCollection OptionControls { get; set; }

    }
}