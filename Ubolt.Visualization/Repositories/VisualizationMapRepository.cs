﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Ubolt.Visualization.Models;

namespace Ubolt.Visualization.Repositories
{
    public class VisualizationMapRepository
    {
        private static readonly VisualizationMapRepository _instance = new VisualizationMapRepository();
        
        public static VisualizationMapRepository Instance
        {
            get { return _instance; }
        }

        internal string ConnectionString
        {
            get { return "Umbraco"; }
        }

        static VisualizationMapRepository()
        {
                
        }
        public VisualizationMapModel Get(int mapID)
        {
            return Get(mapID.ToString());
        }

        public VisualizationMapModel Get(string mapID)
        {
            var db = new Database(ConnectionString);
            return db.SingleOrDefault<VisualizationMapModel>("select * from uboltVisualizationMap where FkNodeID = @0", mapID);
        }

        public VisualizationMapModel Create(int fkNodeID, string alias)
        {
            var db = new Database(ConnectionString);
            var model = new VisualizationMapModel
                {
                    Alias = alias,
                    FkNodeId = fkNodeID,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now,
                    CreatedBy = 0
                };

            db.Insert(model);
            return model;
        }

        public VisualizationMapModel Update(Models.VisualizationMapModel model)
        {
            var db = new Database(ConnectionString);
            db.Update(model);

           
            return model;
        }
    }
}