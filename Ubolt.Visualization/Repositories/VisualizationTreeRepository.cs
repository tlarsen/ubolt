﻿using System.Collections.Generic;
using Ubolt.Visualization.Models;

namespace Ubolt.Visualization.Repositories
{
    public class VisualizationTreeRepository
    {
        private static readonly VisualizationTreeRepository _instance = new VisualizationTreeRepository();

        internal string ConnectionString
        {
            get { return "Umbraco"; }
        }


        public static VisualizationTreeRepository Instance
        {
            get { return _instance; }
        }

        static VisualizationTreeRepository()
        {
            
        }
        public Models.VisualizationTreeModel Create(string name, string template, int parentID)
        {
            var model = new Models.VisualizationTreeModel
                {
                    Name = name,
                    Template = template,
                    ParentID = parentID
                };

            var db = new Database(ConnectionString);
            
            db.Insert(model);
            return model;

        }

        public IEnumerable<Models.VisualizationTreeModel> Get(Models.VisualizationPickerFilterModel filter)
        {
            var db = new Database(ConnectionString);
            return db.Query<Models.VisualizationTreeModel>("select * from UboltVisualizationNode where Template != @0 and Name like @1", "visualizationFolder", string.Concat("%", filter.StartsWith, "%"));
        }


        public IEnumerable<Models.VisualizationTreeModel> GetRootLevel()
        {
            var db = new Database(ConnectionString);
            return db.Query<Models.VisualizationTreeModel>("select * from UboltVisualizationNode where ParentID = @0", -1);
        }

        public IEnumerable<Models.VisualizationTreeModel> GetChildren(int id)
        {
            var db = new Database(ConnectionString);
            return db.Query<Models.VisualizationTreeModel>("select * from UboltVisualizationNode where ParentID = @0", id);
        }

        public bool HasChildren(Models.VisualizationTreeModel model)
        {
            var db = new Database(ConnectionString);
            long count = db.ExecuteScalar<long>("select Count(*) from UboltVisualizationNode where ParentID = @0", model.ID);
            return count > 0;
        }

        public void Delete(int id)
        {
            var db = new Database(ConnectionString);
            db.Execute("delete from UboltVisualizationNode where id = @0", id);
        }

        private IEnumerable<Models.VisualizationTreeModel> LocalRepo()
        {
            var list = new List<Models.VisualizationTreeModel>()
                           {
                               new Models.VisualizationTreeModel(){ID = 1, Name = "Test Top level", ParentID = 0, Template = "visualizationFolder"},
                               new Models.VisualizationTreeModel(){ID = 2, Name = "Test top level 2", ParentID = 0, Template = "visualizationFolder"},
                               new Models.VisualizationTreeModel(){ID = 3, Name = "Top level 3", ParentID = 0, Template = "visualizationFolder"},
                               new Models.VisualizationTreeModel(){ID = 4, Name = "Test", ParentID = 1, Template = "googleChart"},
                               new Models.VisualizationTreeModel(){ID = 5, Name = "Test", ParentID = 1, Template = "googleChart"},
                               new Models.VisualizationTreeModel(){ID = 6, Name = "Test", ParentID = 2, Template = "googleChart"},
                               new Models.VisualizationTreeModel(){ID = 7, Name = "Test", ParentID = 3, Template = "googleChart"},
                               new Models.VisualizationTreeModel(){ID = 8, Name = "Test", ParentID = 3, Template = "googleChart"},
                               new Models.VisualizationTreeModel(){ID = 9, Name = "Folder test", ParentID = 3, Template = "visualizationFolder"},
                               new Models.VisualizationTreeModel(){ID = 10, Name = "Nested folder", ParentID = 9, Template = "visualizationFolder"},
                               new Models.VisualizationTreeModel(){ID = 11, Name = "Troels", ParentID = 10, Template = "googleChart"},
                               new Models.VisualizationTreeModel(){ID = 12, Name = "Test", ParentID = 10, Template = "googleChart"}
                           };
            return list;
        }
    }

}