﻿using System;
using Ubolt.Visualization.Models;

namespace Ubolt.Visualization.Repositories
{
    public class VisualizationChartRepository
    {
        private static readonly VisualizationChartRepository _instance = new VisualizationChartRepository();

        internal string ConnectionString
        {
            get { return "Umbraco"; }
        }


        public static VisualizationChartRepository Instance
        {
            get { return _instance; }
        }

        static VisualizationChartRepository()
        {
            
        }

        
        public Models.VisualizationChartModel Create(int nodeId, string alias, int userId = 0)
        {
            var model = new Models.VisualizationChartModel
                {
                    Alias = alias,
                    FkNodeId = nodeId,
                    CreatedBy = userId,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now
                };

            var db = new Database(ConnectionString);
            db.Insert(model);

            return model;
        }

        public VisualizationChartModel Get(int id)
        {
            return this.Get(id.ToString());
        }

        //TODO: måske sku du lige overveje at det her forvire! Get som henter fra fknodeid måske skal der bare ændres lidt på db scema.
        public VisualizationChartModel Get(string id)
        {
            var db = new Database(ConnectionString);
            return db.SingleOrDefault<Models.VisualizationChartModel>("select * from UboltVisualizationChart where FkNodeId = @0", id);
        }

        //TODO: måske sku du lige overveje at det her forvire! 
        public VisualizationChartModel GetByNodeId(string id)
        {
            var db = new Database(ConnectionString);
            return db.SingleOrDefault<Models.VisualizationChartModel>("select * from UboltVisualizationChart where FkNodeId = @0", id);
        }
        
        public void Delete(int id)
        {
            var db = new Database(ConnectionString);
            int i = db.Execute("delete from UboltVisualizationChart where FkNodeId = @0", id);
        }
       
        public VisualizationChartModel Update(VisualizationChartModel model)
        {
            var db = new Database(ConnectionString);
            db.Update(model);

            return model;
        }

    }

}