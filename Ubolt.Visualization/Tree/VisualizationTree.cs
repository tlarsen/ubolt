using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.BusinessLogic.Actions;
using umbraco.businesslogic;
using umbraco.cms.presentation.Trees;

namespace Ubolt.Visualization.Tree
{
    [Tree("VisualizationApp", "VisualizationTree", "Visualization")]
    public class VisualizationTree : BaseTree
    {
        private List<Models.VisualizationTreeModel> repo;
        public VisualizationTree(string application)
            : base(application)
        {

        }
        protected override void CreateRootNode(ref XmlTreeNode rootNode)
        {
            rootNode.NodeID = "init";
            rootNode.NodeType = "visualizationRoot";
            rootNode.Text = "visualization";
            rootNode.Icon = "chart_bar.png";
            rootNode.OpenIcon = "chart_bar.png";
        }

        public override void Render(ref XmlTree tree)
        {
            

            if (String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["action"]))
            {
                repo = Repositories.VisualizationTreeRepository.Instance.GetRootLevel().ToList();
            }
            else
            {
                repo = Repositories.VisualizationTreeRepository.Instance.GetChildren(int.Parse(HttpContext.Current.Request.QueryString["itemID"])).ToList();
            }

            //this will get returned from the tree service as json for the tree to render.
            foreach (var item in repo)
            {
                XmlTreeNode xmlTreeNode = XmlTreeNode.Create((BaseTree)this);
                xmlTreeNode.Text = item.Name;
                xmlTreeNode.NodeType = item.Template;
                xmlTreeNode.NodeID = item.ID.ToString();
                if (item.Template == "visualizationFolder")
                {
                    xmlTreeNode.Action = string.Format("javascript:UmbClientMgr.contentFrame('/umbraco/plugins/ubolt/charts/ui/editors/ShowFolderContent.aspx?id={0}');", item.ID);
                    xmlTreeNode.OpenIcon = "folder_o.gif";
                    xmlTreeNode.Icon = "folder.gif";
                    xmlTreeNode.Menu.Clear();
                    xmlTreeNode.Menu.Add(ActionNew.Instance);
                    //xmlTreeNode.Menu.Add(Actions.ActionDeleteVisualization.Instance);
                    xmlTreeNode.Menu.Add(ActionRefresh.Instance);
                    
                }

                if (item.Template == "visualizationChart")
                {
                    xmlTreeNode.Action = string.Format("javascript:UmbClientMgr.contentFrame('/umbraco/plugins/ubolt/charts/ui/editors/EditGoogleChart.aspx?id={0}');", item.ID);
                    xmlTreeNode.OpenIcon = "chart_curve.png";
                    xmlTreeNode.Icon = "chart_curve.png";
                    xmlTreeNode.Menu.Clear();
                    //xmlTreeNode.Menu.Add(Actions.ActionDeleteVisualization.Instance);
                    
                }

                if (item.Template == "visualizationMap")
                {
                    xmlTreeNode.Action = string.Format("javascript:UmbClientMgr.contentFrame('/umbraco/plugins/ubolt/charts/ui/editors/EditMap.aspx?id={0}');", item.ID);
                    xmlTreeNode.OpenIcon = "map.png";
                    xmlTreeNode.Icon = "map.png";
                    xmlTreeNode.Menu.Clear();
                    //xmlTreeNode.Menu.Add(Actions.ActionDeleteVisualization.Instance);
                }
                
                //enable the tree service this is the trick to get multi level menus to work 
                //U call the tree service and that calls your render method in this class with the parametericed query u define. so in render u test on that to get u children out.
                if (Repositories.VisualizationTreeRepository.Instance.HasChildren(item))
                    xmlTreeNode.Source = "/umbraco/tree.aspx?action=" + item.Name + "&itemID=" + item.ID + "&app=" + this.m_app + "&treeType=" + HttpContext.Current.Request.QueryString["treeType"] + "&rnd=" + Guid.NewGuid();
                
                tree.Add(xmlTreeNode);
            }
        }

        public override void RenderJS(ref System.Text.StringBuilder Javascript)
        {

        }
    }
   
}