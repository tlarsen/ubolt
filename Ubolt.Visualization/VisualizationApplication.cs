﻿using umbraco.businesslogic;
using umbraco.interfaces;

namespace Ubolt.Visualization
{
    [Application("VisualizationApp", "Visualization", ".traytranslation", 7)]
    public class VisualizationApplication : IApplication
    {
        public VisualizationApplication()
        {
            
        }
    }
}