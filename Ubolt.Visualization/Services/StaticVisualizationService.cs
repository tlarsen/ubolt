﻿using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Ubolt.Visualization.Models;

namespace Ubolt.Visualization.Services
{
    public class StaticVisualizationService
    {
        private string _imagePath;

        private static StaticVisualizationService _instance = null;
        public static StaticVisualizationService Instance
        {
            get { return _instance ?? (_instance = new StaticVisualizationService()); }
        }

        internal StaticVisualizationService()
        {
            _imagePath = ConfigurationManager.AppSettings.Get("Ubolt.Charts.StaticImagePath") != string.Empty ? ConfigurationManager.AppSettings.Get("Ubolt.Charts.StaticImagePath") : "/img/charts/";
        }
        
        internal string CreateImage(string base64Data, int chartId)
        {
            base64Data = base64Data.Replace("data:image/png;base64,", "");
            string imagePath = string.Format("{0}{1}.png", _imagePath, chartId);
            
            byte[] imageBytes = Convert.FromBase64String(base64Data);
            using (var ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
            {
                ms.Write(imageBytes, 0, imageBytes.Length);

                var image = System.Drawing.Image.FromStream(ms);
                image.Save(HttpContext.Current.Server.MapPath(imagePath), System.Drawing.Imaging.ImageFormat.Png);
            }
            return imagePath;
        }

        public string GetImagePath(int chartId)
        {
            return string.Format("{0}{1}.png", _imagePath, chartId);
        }

        public VisualizationChartModel GetStaticChart(int chartId)
        {
            return Repositories.VisualizationChartRepository.Instance.Get(chartId);
        }

        private byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = MD5.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        private string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

    }
}