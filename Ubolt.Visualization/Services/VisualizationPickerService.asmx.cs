﻿using System.Collections.Generic;
using System.Web.Services;
using System.Xml.Linq;
using System.Linq;
using Newtonsoft.Json;

namespace Ubolt.Visualization.Services
{
    /// <summary>
    /// Summary description for ChartPickerService
    /// </summary>
    [WebService(Namespace = "http://agenda.dk/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class VisualizationPickerService : System.Web.Services.WebService
    {

        /// <summary>
        /// Used by auto complete 
        /// </summary>
        /// <param name="startsWith"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetCharts(string startsWith)
        {
            var filter = new Models.VisualizationPickerFilterModel()
            {
                StartsWith = startsWith,
                IncludeFolders = false
            };

            var models = Repositories.VisualizationTreeRepository.Instance.Get(filter);

            return JsonConvert.SerializeObject(models);
        }

        [WebMethod]
        public string GetChartData(string id)
        {
            var model = Repositories.VisualizationChartRepository.Instance.Get(id);
            if (model == null)
                return string.Empty;

            return JsonConvert.SerializeObject(model);
        }

        [WebMethod]
        public string GetMapData(string id)
        {
            var model = Repositories.VisualizationMapRepository.Instance.Get(id);
            if (model == null)
                return string.Empty;

            XDocument xml = XDocument.Load(Server.MapPath("/paths.xml"));
                
            dynamic data = JsonConvert.DeserializeObject(model.Data);
            foreach (dynamic row in data.data)
            {
                var path = xml.Descendants("path").FirstOrDefault(x => x.Attribute("id").Value == row.id.ToString());
                if (path != null)
                {
                    row.path = path.Value;
                }
            }
            model.Data = JsonConvert.SerializeObject(data, Formatting.None);
            return JsonConvert.SerializeObject(model);
        }

    }
}
