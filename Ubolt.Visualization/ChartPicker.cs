﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using umbraco.IO;

namespace Ubolt.Visualization
{
    public class ChartPicker : PlaceHolder
    {
        private bool FirstLoad
        {
            get
            {
                if (this.ViewState["FirstLoad"] == null)
                    this.ViewState["FirstLoad"] = (object)true;
                return Convert.ToBoolean(this.ViewState["FirstLoad"]);
            }
            set
            {
                this.ViewState["FirstLoad"] = Convert.ToBoolean(value ? 1 : 0);

            }
        }

        private string _path = IOHelper.ResolveUrl(SystemDirectories.Umbraco) + "/plugins/Ubolt/Charts/UI/ChartPickerControl.ascx";
        private object _dataValue;

        public object DataValue
        {
            get { return ((UI.ChartPickerControl)Controls[0]).value;  }
            set { _dataValue = value; }
        }

        public object Model { get; set; }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.Controls.Add(new UserControl().LoadControl(_path));
        }

        protected override void OnLoad(EventArgs e)
        {

            if (!this.FirstLoad)
                return;

            ((UI.ChartPickerControl)Controls[0]).value = _dataValue;
            if (_dataValue != null)
            {
                var model = Repositories.VisualizationChartRepository.Instance.Get(_dataValue.ToString());
                if (model != null)
                    ((UI.ChartPickerControl)Controls[0]).model = model;
 
            }
           
            this.FirstLoad = false;

            //((UI.ChartPickerControl) Controls[0]).value = _dataValue;
            
        }
       
        
    }
}