﻿using System;
using umbraco.editorControls.userControlGrapper;

namespace Ubolt.Visualization.UI
{
    public partial class ChartPickerControl : System.Web.UI.UserControl, IUsercontrolDataEditor
    {
        private string _value;
        public object value
        {
            get { return HiddenChartId.Value; }
            set
            {
                if (value != null)
                    _value = value.ToString();
            }
        }

        public object model { get; set; }
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(_value))
            {
                HiddenChartId.Value = _value;

                
                if(model != null)
                   selectedChart.InnerText = ((Models.VisualizationChartModel)model).Alias;
            }
                

            
        }

    }
}