using System;
using umbraco.uicontrols;

namespace Ubolt.Visualization.UI.Editors
{
    public partial class ShowFolderContent : System.Web.UI.Page
    {
        protected TabPage FirstTab;
        protected TabPage SecondTab;


        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected override void OnInit(EventArgs e)
        {
            FirstTab = tabControl.NewTabPage("Folder Content");
            FirstTab.Controls.Add(PaneData);


            SecondTab = tabControl.NewTabPage("Poperties");
            SecondTab.Controls.Add(PanePropperties);

        }
    }
}