using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ubolt.Visualization.Models;
using umbraco.BasePages;
using umbraco.uicontrols;

namespace Ubolt.Visualization.UI.Editors
{
    public partial class EditGoogleChart : UmbracoEnsuredPage
    {
        public ImageButton saveButton;
        public ImageButton resetButton;
        public ImageButton deleteButton;

        
        public string UniqueChartId { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UniqueChartId = Guid.NewGuid().ToString();
                PopulateChartSettings(Convert.ToInt32(Request.QueryString["id"]));
            }
        }

        protected override void OnInit(EventArgs e)
        {


            saveButton = umbPanel.Menu.NewImageButton();
            saveButton.ImageUrl = string.Format("{0}/images/editor/save.gif", umbraco.GlobalSettings.Path);
            saveButton.ToolTip = "Save chart data";
            saveButton.Click += btnSave_Click;
            saveButton.ID = "save";

            resetButton = umbPanel.Menu.NewImageButton();
            resetButton.ImageUrl = string.Format("{0}/images/editor/upload.png", umbraco.GlobalSettings.Path);
            resetButton.ToolTip = "Reset chart data";
            resetButton.OnClientClick = "return confirm('Are you sure u want to reset all chart data ?');";
            resetButton.Click += btnReset_Click;
            
            deleteButton = umbPanel.Menu.NewImageButton();
            deleteButton.ImageUrl = string.Format("{0}/images/editor/delete.png", umbraco.GlobalSettings.Path);
            deleteButton.ToolTip = "delete chart";
            deleteButton.OnClientClick = "return confirm('Are you sure u want to delete this chart ?');";
            deleteButton.Click += btnDelete_Click;
            base.OnInit(e);
        }

        void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            DeleteChart(Convert.ToInt32(Request.QueryString["id"]));
        }

        void btnReset_Click(object sender, ImageClickEventArgs e)
        {
            ResetChart(Convert.ToInt32(Request.QueryString["id"]));
        }

        void btnSave_Click(object sender, ImageClickEventArgs e)
        {
            SaveChart(Convert.ToInt32(Request.QueryString["id"]));
        }
        protected void DeleteChart(int chartId)
        {
            var model = Repositories.VisualizationChartRepository.Instance.Get(chartId);
            
            Repositories.VisualizationChartRepository.Instance.Delete(chartId);
            Repositories.VisualizationTreeRepository.Instance.Delete(chartId);

            string path = string.Format("/umbraco/plugins/ubolt/charts/ui/editors/ShowFolderContent.aspx?id={0}", model.FkNodeId);

            ClientTools.RefreshTree();
            ClientTools.ChangeContentFrameUrl(path);
        }

        protected void ResetChart(int chartId)
        {
            var model = new Models.VisualizationChartModel
                {
                    Id = chartId,
                    UpdateDate = DateTime.Now,
                    Type = 1,
                    Alias = string.Empty,
                    Data = string.Empty,
                    Description = string.Empty,
                    HoristontalAxis = string.Empty,
                    VerticalAxis = string.Empty,
                    Notes = string.Empty,
                    Source = string.Empty
                };

            Repositories.VisualizationChartRepository.Instance.Update(model);

            ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.info, "Chart reset", "Any errors are gone now.");

        }

        protected void PopulateChartSettings(int chartId)
        {
            var chart = Repositories.VisualizationChartRepository.Instance.Get(chartId);
            if (chart != null)
            {
                //updateing preview values.
                chart_title.InnerText = chart.Alias;
                chart_description.InnerText = chart.Source;

                //updating textboxes.
                tbChartTitle.Text = chart.Alias;
                tbHorisontalTitle.Text = chart.HoristontalAxis;
                tbVerticalTitle.Text = chart.VerticalAxis;
                tbChartSource.Text = chart.Source;
                tbChartDescription.Text = chart.Description;
                tbChartNotes.Text = chart.Notes;

                //setting data that gets send to google. and used by the editor to populate fields.
                javaScriptArrayHidden.Value = chart.Data;
            }
        }

        protected void SaveChart(int chartId)
        {
            var model = Repositories.VisualizationChartRepository.Instance.Get(chartId);
            model.Alias = tbChartTitle.Text;
            model.HoristontalAxis = tbHorisontalTitle.Text;
            model.VerticalAxis = tbVerticalTitle.Text;
            model.Data = javaScriptArrayHidden.Value;
            model.Description = tbChartDescription.Text;
            model.StaticImagePath = Services.StaticVisualizationService.Instance.CreateImage(Base64ChartHidden.Value, chartId);
            model.Notes = tbChartNotes.Text;
            model.Source = tbChartSource.Text;
            model.UpdateDate = DateTime.Now;
            //model.Type = 1;

            Repositories.VisualizationChartRepository.Instance.Update(model);

            ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.save, "Chart saved", "And ready to be used on content pages.");
        }
    }
}