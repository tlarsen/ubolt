<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditMap.aspx.cs" Inherits="Ubolt.Visualization.UI.Editors.EditMap" MasterPageFile="/Umbraco/masterpages/umbracoPage.Master" Title="Charts" %>

<%@ Register TagPrefix="umb" Namespace="umbraco.uicontrols" Assembly="controls" %>
<%@ Register TagPrefix="client" Namespace="ClientDependency.Core.Controls" Assembly="ClientDependency.Core" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="head">
    <client:CssInclude ID="cssHandsonTable" runat="server" FilePath="plugins/Ubolt/Charts/UI/Javascript/handsontable/jquery.handsontable.css" PathNameAlias="UmbracoRoot" />
    <client:JsInclude ID="jsHandsonTable" runat="server" FilePath="plugins/Ubolt/Charts/UI/Javascript/handsontable/jquery.handsontable.js" PathNameAlias="UmbracoRoot" />

    <script type="text/javascript" src="../Javascript/uMaps.js"></script>

    <script type="text/javascript">
        $(function () {
            UmbClientMgr.appActions().bindSaveShortCut();

            uMapComponentHandler.initUMaps(new uMap({
                handsonTableId: "#dataTable",
                hiddenSettingsId: "#<%= hiddenData.ClientID %>",
                legendSettingsClass: ".legendSetting_<%= UniqueMapId %>"
            }));

        });
    </script>

</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="body">
    <umb:TabView runat="server" ID="tabView" Width="552px" Height="692px" />

    <umb:Pane runat="server" ID="PaneSettings">

        <%--
            <a class="dashboardHideLink" onclick="if(confirm('Are you sure you want remove this dashboard item?')){jQuery(this).closest('.propertypane').fadeOut();return false;}">- Hide</a>  
            <div class="dashboardWrapper">
                <h2>Map editor</h2>
                <img src="/umbraco/dashboard/images/logo32x32.png" alt="Umbraco" class="dashboardIcon">
                <p>Standard settings</p>
                <div class="dashboardColWrapper">
                    <div class="dashboardCols">
                        <div class="dashboardCol third">
                            <h3>Aixis</h3>
                            Title<input type="text"/><br/>
                            Description<input type="text"/>
                        </div>
                        <div class="dashboardCol third">
                            <h3>Go further</h3>
                             Title<input type="text"/><br/>
                            Description<input type="text"/>
                        </div>
                        <div class="dashboardCol third last">
                            <h3>Go further</h3>
                            Title<input type="text"/><br/>
                             Description<input type="text"/>
                            
                        </div>
                    </div>
                </div>
            </div>--%>

        <umb:PropertyPanel runat="server" Text="Map title">
            <asp:TextBox ID="tbMapTitle" runat="server" CssClass="guiInputText" Width="230px"></asp:TextBox>
        </umb:PropertyPanel>

        <umb:PropertyPanel runat="server" Text="Map description">
            <asp:TextBox runat="server" ID="tbMapDescription" CssClass="guiInputText" Width="230px" TextMode="MultiLine"></asp:TextBox>
        </umb:PropertyPanel>

        <umb:PropertyPanel runat="server" Text="Map notes">
            <asp:TextBox runat="server" ID="tbMapNotes" CssClass="guiInputText" Width="230px" TextMode="MultiLine"></asp:TextBox>
        </umb:PropertyPanel>

        <umb:PropertyPanel runat="server" Text="Map Source">
            <asp:TextBox runat="server" ID="tbMapSource" CssClass="guiInputText" Width="230px"></asp:TextBox>
            <asp:TextBox ID="tbMapData" runat="server" TextMode="MultiLine" Height="300px" Width="700px" Style="display: none"></asp:TextBox>
        </umb:PropertyPanel>
    </umb:Pane>

  <umb:Pane runat="server" ID="PaneType">
        <umb:PropertyPanel runat="server" Text="Map Type">
            <asp:DropDownList runat="server">
                <asp:ListItem Text="Kommune kort" Value="municipal" Selected="True"></asp:ListItem>
            </asp:DropDownList>
        </umb:PropertyPanel>
    </umb:Pane>
    
    <umb:Pane runat="server" ID="PaneLegends">
        <umb:PropertyPanel runat="server" Text="Map Legends">
            <div>
                <label>Legend</label> 
                <input type="text" class="legendSetting_<%= UniqueMapId %> guiInputText" data-key="text" data-id="1"/> 
                <label>farve </label> 
                <select name="legends" class="legendSetting_<%= UniqueMapId %> guiInputText" data-key="color" data-id="1">
                    <option value="#0a4167">M�rk Bl�</option>
                    <option value="#47718d">Lys bl�</option>
                    <option value="#666666">Gr�</option>
                    <option value="#a39262">Sand</option>
                    <option value="#afae9a">Sandgr�</option>
                    <option value="#dbdbd3">Lys gr�</option>
                </select>
            </div>
            
            <div>
                <label>Legend</label> 
                <input type="text" class="legendSetting_<%= UniqueMapId %> guiInputText" data-key="text" data-id="2"/> 
                <label>farve </label> 
                <select name="legends" class="legendSetting_<%= UniqueMapId %> guiInputText" data-key="color" data-id="2">
                    <option value="#0a4167">M�rk Bl�</option>
                    <option value="#47718d">Lys bl�</option>
                    <option value="#666666">Gr�</option>
                    <option value="#a39262">Sand</option>
                    <option value="#afae9a">Sandgr�</option>
                    <option value="#dbdbd3">Lys gr�</option>
                </select>
            </div>
            
             <div>
                <label>Legend</label> 
                <input type="text" class="legendSetting_<%= UniqueMapId %> guiInputText" data-key="text" data-id="3"/> 
                <label>farve </label> 
                <select name="legends" class="legendSetting_<%= UniqueMapId %> guiInputText" data-key="color" data-id="3">
                    <option value="#0a4167">M�rk Bl�</option>
                    <option value="#47718d">Lys bl�</option>
                    <option value="#666666">Gr�</option>
                    <option value="#a39262">Sand</option>
                    <option value="#afae9a">Sandgr�</option>
                    <option value="#dbdbd3">Lys gr�</option>
                </select>
            </div>
            	
	

             <div>
                <label>Legend</label> 
                <input type="text" class="legendSetting_<%= UniqueMapId %> guiInputText" data-key="text" data-id="4"/> 
                <label>farve </label> 
                <select name="legends" class="legendSetting_<%= UniqueMapId %> guiInputText" data-key="color" data-id="4">
                     <option value="#0a4167">M�rk Bl�</option>
                    <option value="#47718d">Lys bl�</option>
                    <option value="#666666">Gr�</option>
                    <option value="#a39262">Sand</option>
                    <option value="#afae9a">Sandgr�</option>
                    <option value="#dbdbd3">Lys gr�</option>
                </select>
            </div>
            
             <div>
                <label>Legend</label> 
                <input type="text" class="legendSetting_<%= UniqueMapId %> guiInputText" data-key="text" data-id="5"/> 
                <label>farve </label> 
                <select name="legends" class="legendSetting_<%= UniqueMapId %> guiInputText" data-key="color" data-id="5">
                      <option value="#0a4167">M�rk Bl�</option>
                    <option value="#47718d">Lys bl�</option>
                    <option value="#666666">Gr�</option>
                    <option value="#a39262">Sand</option>
                    <option value="#afae9a">Sandgr�</option>
                    <option value="#dbdbd3">Lys gr�</option>
                </select>
            </div>
        </umb:PropertyPanel>
    </umb:Pane>

    <umb:Pane runat="server" ID="PaneData">
        <umb:PropertyPanel runat="server" Text="Map Data">
            <div id="dataTable" class="dataTable"></div>
        </umb:PropertyPanel>
    </umb:Pane>
    
    
     <umb:Pane runat="server" ID="PaneGeneralProperties">
        <umb:PropertyPanel ID="PropertyPanel1" runat="server" Text="Node name">
            <asp:TextBox runat="server" ID="generalPropertiesNodeName" Width="400px"></asp:TextBox>
        </umb:PropertyPanel>
         <umb:PropertyPanel ID="PropertyPanel2" runat="server" Text="Created by">
            <asp:Label runat="server" ID="generalPropertiesCreatedBy"></asp:Label>
        </umb:PropertyPanel>
         <umb:PropertyPanel ID="PropertyPanel4" runat="server" Text="Created">
            <asp:Label runat="server" ID="generalPropertiesCreated"></asp:Label>
        </umb:PropertyPanel>
         <umb:PropertyPanel ID="PropertyPanel5" runat="server" Text="Id">
            <asp:Label runat="server" ID="generalPropertiesID"></asp:Label>
        </umb:PropertyPanel>
         <umb:PropertyPanel ID="PropertyPanel3" runat="server" Text="Link to static image">
            <asp:Label runat="server" ID="generalPropertiesStaticImagePath"></asp:Label>
             <%--<a href="http://ubolt.demosite.dev/temp/charts/1.png" target="_blank">/temp/charts/1.png</a>--%>
        </umb:PropertyPanel>
    </umb:Pane>
    
    <asp:HiddenField runat="server" ID="hiddenData"/>
</asp:Content>



