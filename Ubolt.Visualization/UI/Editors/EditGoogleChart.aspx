<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditGoogleChart.aspx.cs" Inherits="Ubolt.Visualization.UI.Editors.EditGoogleChart" MasterPageFile="/Umbraco/masterpages/umbracoPage.Master" Title="Charts" %>

<%@ Register TagPrefix="umb" Namespace="umbraco.uicontrols" Assembly="controls" %>
<%@ Register TagPrefix="client" Namespace="ClientDependency.Core.Controls" Assembly="ClientDependency.Core" %>
<asp:Content runat="server" ContentPlaceHolderID="head">
    <client:CssInclude ID="cssHandsonTable" runat="server" FilePath="plugins/Ubolt/Charts/UI/Css/jquery.handsontable.css" PathNameAlias="UmbracoRoot" />
    <client:CssInclude ID="cssUboltCharts" runat="server" FilePath="plugins/Ubolt/Charts/UI/Css/ubolt.charts.css" PathNameAlias="UmbracoRoot" />
    <client:JsInclude ID="jsHandsonTable" runat="server" FilePath="plugins/Ubolt/Charts/UI/Javascript/handsontable.js" PathNameAlias="UmbracoRoot" />
    <client:JsInclude ID="jsGoogleChartsAPI" runat="server" FilePath="http://www.google.com/jsapi" />
    <script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/rgbcolor.js"></script>
    <script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/canvg.js"></script>
    <script type="text/javascript" src="../Javascript/uCharts.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <umb:UmbracoPanel ID="umbPanel" hasMenu="True" runat="server">
        <umb:Pane runat="server" ID="PaneSettings">
            <umb:PropertyPanel ID="PropertyPanel1" runat="server" Text="Chart titel">
                <asp:TextBox ID="tbChartTitle" runat="server" CssClass="guiInputText" Width="230px"></asp:TextBox>
            </umb:PropertyPanel>
            
            <umb:PropertyPanel ID="PropertyPanel2" runat="server" Text="Chart description">
                <asp:TextBox runat="server" ID="tbChartDescription" CssClass="guiInputText" Width="230px" TextMode="MultiLine"></asp:TextBox>
            </umb:PropertyPanel>

            <umb:PropertyPanel ID="PropertyPanel3" runat="server" Text="Chart Horizontal Axis Title ">
                <asp:TextBox ID="tbHorisontalTitle" runat="server" Width="230px"  style="display:none;"></asp:TextBox>
                <input type="text" id="chartHAxisTitle" data-key="hAxis.title" class="chartSetting_<%= UniqueChartId %> guiInputText" style="width: 230px"/>
            </umb:PropertyPanel>

            <umb:PropertyPanel ID="PropertyPanel4" runat="server" Text="Chart Vertical Axis Title ">
                <asp:TextBox ID="tbVerticalTitle" runat="server" Width="230px" style="display:none;"></asp:TextBox>
                <input type="text" id="chartVAxisTitle" data-key="vAxis.title" class="chartSetting_<%= UniqueChartId %> guiInputText" style="width: 230px"/>
            </umb:PropertyPanel>
                    
            <umb:PropertyPanel ID="PropertyPanel5" runat="server" Text="Chart Source">
                <asp:TextBox runat="server" ID="tbChartSource" CssClass="guiInputText" Width="230px"></asp:TextBox>
            </umb:PropertyPanel>
            
            <umb:PropertyPanel ID="PropertyPanel6" runat="server" Text="Chart notes">
                <asp:TextBox runat="server" ID="tbChartNotes" CssClass="guiInputText" Width="230px" TextMode="MultiLine"></asp:TextBox>
            </umb:PropertyPanel>
        </umb:Pane>
        
        <umb:Pane runat="server" ID="PaneData">
            <umb:PropertyPanel ID="PropertyPanel7" runat="server" Text="Chart Data">
                <div id="dataTable_<%= UniqueChartId %>" class="dataTable" style="width: 700px; height: 200px; overflow: scroll"></div>
            </umb:PropertyPanel>
        </umb:Pane>

        <umb:Pane runat="server" ID="PaneType">
            <umb:PropertyPanel ID="PropertyPanel8" runat="server" Text="Chart type">
                <div class="chartType">
                    <input type="radio" id="column" name="chart_<%= UniqueChartId %>" value="column" title="Column Chart" data-key="Column" class="chartChooser_<%= UniqueChartId %>" />
                    <label for="column">
                        <img src="/umbraco/plugins/Ubolt/Charts/UI/Images/chart_column.png" alt="Column Chart" />
                        Column Chart
                    </label>
                </div>

                <div class="chartType">
                    <input type="radio" id="bar" name="chart_<%= UniqueChartId %>" value="bar" title="Bar Chart" data-key="Bar" class="chartChooser_<%= UniqueChartId %>" />
                    <label for="bar">
                        <img src="/umbraco/plugins/Ubolt/Charts/UI/Images/chart_bar.png" alt="Bar Chart" />
                        Bar Chart
                    </label>
                </div>

                <div class="chartType">
                    <input type="radio" id="line" name="chart_<%= UniqueChartId %>" value="line" title="Line Chart" data-key="Line" class="chartChooser_<%= UniqueChartId %>" />
                    <label for="line">
                        <img src="/umbraco/plugins/Ubolt/Charts/UI/Images/chart_line.png" alt="Line Chart" />
                        Line Chart
                    </label>
                </div>
                
                <div class="chartType">
                    <input type="radio" id="pie" name="chart_<%= UniqueChartId %>" value="pie" title="Pie Chart" data-key="Pie" class="chartChooser_<%= UniqueChartId %>" />
                    <label for="pie">
                        <img src="/umbraco/plugins/Ubolt/Charts/UI/Images/chart_pie.png" alt="Pie Chart" />
                        Pie Chart
                    </label>
                </div>

                 <div class="chartType">
                    <input type="radio" id="area" name="chart_<%= UniqueChartId %>" value="area" title="Area Chart" data-key="Area" class="chartChooser_<%= UniqueChartId %>" />
                    <label for="area">
                        <img src="/umbraco/plugins/Ubolt/Charts/UI/Images/chart_column.png" alt="Column Chart" />
                        Area
                    </label>
                </div>
                
                 <%--<div>
                    <input type="radio" id="org" name="chart_<%= UniqueChartId %>" value="org" title="Orgenasation Diagram" data-key="Org" class="chartChooser_<%= UniqueChartId %>" />
                    <label for="org">
                        <img src="/umbraco/plugins/Ubolt/Charts/UI/Images/chart_column.png" alt="Org Chart" />
                       Orgenasation Diagram
                </label>
                </div>--%>
                
                <%--<div>

                    <input type="radio" id="curve" name="chart_<%= UniqueChartId %>" value="curve" title="Curve Chart" data-key="Curve" class="chartChooser_<%= UniqueChartId %>" />
                    <label for="curve">
                        <img src="/umbraco/plugins/Ubolt/Charts/UI/Images/chart_curve.png" alt="Curve Chart" />
                        Curve Chart
                </label>
                </div>--%>
            </umb:PropertyPanel>
           
        </umb:Pane>
         <umb:Pane ID="Pane1" runat="server">
              <umb:PropertyPanel ID="PaneIsStacked" runat="server" Text="Style Settings">
                   <div id="forceHAxisZero">
                    <label for="forceHZero">Force horisontal axis to zero</label>
                    <input type="checkbox" id="forceHZero" name="hZero_<%= UniqueChartId %>" value="false" title="Yes" data-key="hAxis.minValue" class="chartSetting_<%= UniqueChartId %>" />
                </div>
                  
                 <div id="forceVAxisZero">
                    <label for="forceVZero">Force Vertical axis to zero</label>
                    <input type="checkbox" id="forceVZero" name="vZero_<%= UniqueChartId %>" value="false" title="Yes" data-key="vAxis.minValue" class="chartSetting_<%= UniqueChartId %>" />
                </div>

                  
                  <div id="stacked">
                    <label for="stackedYes">Stack values</label>
                    <input type="checkbox" id="stackedYes" name="stacked_<%= UniqueChartId %>" value="true" title="Yes" data-key="isStacked" class="chartSetting_<%= UniqueChartId %>" />
                </div>

                
                  
                <div id="slantedText">
                    <label for="slantedTextAngle">Angle of text</label>
                    <select id="slantedTextAngle" name="slanted_<%= UniqueChartId %>"  class="chartSetting_<%= UniqueChartId %>" data-key="hAxis.slantedTextAngle">
                        <option value="0">0 &deg;</option>
                        <option value="30">30&deg;</option>
                        <option value="60">60&deg;</option>
                        <option value="90">90&deg;</option>
                    </select>
                </div>
                <div id="fontSize">
                    <label>Size of font</label>
                    <select name="fontSize_<%= UniqueChartId %>"  class="chartSetting_<%= UniqueChartId %>" data-key="fontSize">
                        <option value="8">8 pt</option>
                        <option value="10">10 pt</option>
                        <option value="12" selected="selected">12 pt</option>
                        <option value="14">14 pt</option>
                    </select>
                </div>
                <div id="legend">
                    <label>Legend</label>
                    <select name="legend_<%= UniqueChartId %>"  class="chartSetting_<%= UniqueChartId %>" data-key="legend.position">
                        <option value="bottom" selected="selected">bottom</option>
                        <option value="top">top</option>
                        <option value="none">none</option>
                    </select>
                </div>
            </umb:PropertyPanel>
         </umb:Pane>

        <umb:Pane runat="server" ID="PanePreview">
            <umb:PropertyPanel ID="PropertyPanel9" runat="server" Text="Chart Preview">
                <div id="chart_title" runat="server" style="background-color: #ecf1f5; color: #094167;font-size: 3.76923076923077em;line-height: 1.16326530612245em;font-family: 'Merriweather', serif;font-weight: 400;padding-left: 20px;"></div>
                <div id="chart_div" runat="server" style="width: 900px; height: 400px;"></div>
                <div id="chart_description" runat="server" style="background-color: #ecf1f5; color: #094167;font-size: 0.96923076923077em;line-height: 1.16326530612245em;font-family: 'Merriweather', serif;font-weight: 400;padding-left: 20px; font-style: italic"></div>
            </umb:PropertyPanel>
        </umb:Pane>
           
        <umb:Pane ID="Pane2" runat="server">
            <umb:PropertyPanel ID="PaneMin" runat="server" Text="Data offset">
                <div id="offset">
                     <label for="viewModeMin">
                        Start offset
                    </label> 
                    <input type="text" id="viewModeMin" data-key="hAxis.viewWindow.min" class="chartSetting_<%= UniqueChartId %>" style="width: 30px"/>
                
                    <label for="viewModeMax">
                        End offset 
                    </label> 
                    <input type="text" id="viewModeMax" data-key="hAxis.viewWindow.max" class="chartSetting_<%= UniqueChartId %>" style="width: 30px"/>
                </div>   
            </umb:PropertyPanel>
         </umb:Pane>
    </umb:UmbracoPanel>
    
    <asp:HiddenField runat="server" ID="javaScriptArrayHidden"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="Base64ChartHidden"></asp:HiddenField>
    <script type="text/javascript">

        $(function () {

            UmbClientMgr.appActions().bindSaveShortCut();

            uChartComponentHandler.initUCharts(new uChart({
                saveButton: "#<%= ((Control)saveButton).ClientID %>",
                hiddenBase64Id: "#<%= Base64ChartHidden.ClientID %>",
                hiddenSettingsId: "#<%= javaScriptArrayHidden.ClientID %>",
                handsonTableId: "#dataTable_<%= UniqueChartId %>",
                googleChartDivId: "#<%= chart_div.ClientID %>",
                chartSettingsClass: ".chartSetting_<%= UniqueChartId %>",
                chartTypeClass: ".chartChooser_<%= UniqueChartId %>"
            }));

            $("#<%= tbVerticalTitle.ClientID %>").val($("#chartVAxisTitle").val());
            $("#<%= tbHorisontalTitle.ClientID %>").val($("#chartHAxisTitle").val());


        });
    </script>

</asp:Content>
