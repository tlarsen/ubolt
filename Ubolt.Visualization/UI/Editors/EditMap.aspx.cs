using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using umbraco.BasePages;
using umbraco.uicontrols;

namespace Ubolt.Visualization.UI.Editors
{
    public partial class EditMap : UmbracoEnsuredPage
    {

        protected ImageButton saveButton;
        protected ImageButton deleteButton;

        protected TabPage contentPage;
        protected TabPage propertiesPage;

        public string UniqueMapId { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UniqueMapId = Guid.NewGuid().ToString();
                PopulateMapSettings(Convert.ToInt32(Request.QueryString["id"]));
            }
        }

        protected override void OnInit(EventArgs e)
        {
            contentPage = tabView.NewTabPage("Content");
            contentPage.Controls.Add(PaneSettings);
            contentPage.Controls.Add(PaneType);
            contentPage.Controls.Add(PaneLegends);
            contentPage.Controls.Add(PaneData);
            AddSaveButton(ref contentPage);

            propertiesPage = tabView.NewTabPage("Properties");
            propertiesPage.Controls.Add(PaneGeneralProperties);
            AddSaveButton(ref propertiesPage);
        }

        private void PopulateMapSettings(int mapID)
        {
            var map = Repositories.VisualizationMapRepository.Instance.Get(mapID);
            if (map != null)
            {
                tbMapTitle.Text = map.Alias;
                tbMapDescription.Text = map.Description;
                tbMapSource.Text = map.Source;
                tbMapNotes.Text = map.Notes;
                hiddenData.Value = map.Data;
            }
        }

        private void AddSaveButton(ref TabPage page)
        {
            saveButton = page.Menu.NewImageButton();
            saveButton.ImageUrl = string.Format("{0}/images/editor/save.gif", umbraco.GlobalSettings.Path);
            saveButton.ToolTip = "Save map";
            saveButton.Click += btnSave_Click;
            saveButton.ID = string.Concat(page.ClientID, "_save");
        }

        void btnSave_Click(object sender, ImageClickEventArgs e)
        {
            SaveMap(Convert.ToInt32(Request.QueryString["id"]));            
        }

        private void SaveMap(int mapId)
        {
            if (Page.IsValid)
            {
                var model = Repositories.VisualizationMapRepository.Instance.Get(mapId);
                model.Alias = tbMapTitle.Text;
                model.Description = tbMapDescription.Text;
                model.Notes = tbMapNotes.Text;
                model.Source = tbMapSource.Text;
                model.UpdateDate = DateTime.Now;
                
                model.Data = hiddenData.Value;

                Repositories.VisualizationMapRepository.Instance.Update(model);
                ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.save, "Map saved", "And ready to be used on content pages.");
            }
        }
    }
}