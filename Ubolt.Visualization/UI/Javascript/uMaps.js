﻿(function (window, $) {
    "use strict";
    var uMap, uMapDataHandler, uMapRenderHandler, uMapComponentHandler;

    window.uMap = uMap = (function () {

        function Map(obj) {
            var key,
                value,
                defaults = {
                    settings: {
                        data: [],
                        legends: [],
                    }
                };

            obj = $.extend(obj, defaults);

            for (key in obj) {
                value = obj[key];
                this[key] = value;
            }
        };
        return Map;

    })();

    window.uMapDataHandler = uMapDataHandler = (function () {
      
        var setData = function (map) {
            var dataTable = $(map.handsonTableId);
            map.settings.data = dataTable.handsontable('getData');
            $(map.hiddenSettingsId).val(JSON.stringify(map.settings));

        };
        
        var setLegends = function (control, map) {

            var $control = $(control), value;
            var key = $control.data("key");
            var id = $control.data("id");
            
            if (key === null || key === undefined) {
                return;
            }
            key = key.replace(/\s/g, "");
            var keys = key.split(/,/g);

            value = getValueFromElement.call(control);

            updateLegends(keys, value, map, id);

            $(map.hiddenSettingsId).val(JSON.stringify(map.settings));
            //uChartRenderHandler.updateChart(chart);
        };

        var getValueFromElement = function () {

            switch (this.type) {
                case "text":
                    if (!/[^,\.\d\s]+/g.test(this.value)) {
                        return parseFloat(this.value);
                    }
                    return (this.value);
                case "checkbox":
                    return this.checked;
                case "radio":
                    return this.checked;
                case "select-one":
                    return this.value;
                default:
                    return;
            }
        };

        var updateLegends = function (keys, value, self, id) {
            var obj;
            $.each(keys, function () {

                if (!keys || !keys.length) {
                    return;
                }

                if (!/\./g.test(this)) {
                    
                    $.each(self.settings.legends, function (i) {
                        
                        if (this.id == id) {
                            obj = self.settings.legends[i];
                            return false;
                        } 
                    });

                    // legends is null, create obj
                    if (!obj) {
                        //init new legend object
                        obj = { id: id, text: '', color: '' };
                        //push to legends array
                        self.settings.legends.push(obj);
                    }
                    //set value after we are sure that object is not null
                    obj[this] = value;
                    
                    
                }
           });
        };
        

        return {
            setData: setData,
            setLegends: setLegends
        };

    })();

    window.uMapRenderHandler = uMapRenderHandler = (function () {

        //make redraw - update function til henrik så vi kan få live preview i backend
          
    })();

    window.uMapComponentHandler = uMapComponentHandler = (function () {

        var initLegendSettings = function (map) {
            $(map.legendSettingsClass).each(function () {
                var $this = $(this), key = $this.data("key"), id = $this.data("id");
                console.log(key);
                if (key === null || key === undefined) {
                    return true;
                }

                key = key.replace(/\s/g, "").split(/,/g)[0];
                
                if (!/\./g.test(key)) {
                   
                    $.each(map.settings.legends, function() {
                        if (this.id == id) {
                            $this.val(this[key]);        
                        }
                    });
                    
                }
                else {
                    var arrayKey = key.split(/\./g),
                        obj = map.settings.legends;

                    $.each(arrayKey, function () {
                        if (obj) {
                            obj = obj[this];
                        }
                    });

                    if (obj) {
                        $this.val(obj);
                    }
                }
            });
        };

        var initHandsonTable = function (map) {
             
            var $dataTable = $(map.handsonTableId);
            
            $dataTable.handsontable({
                minSpareRows: 1,
                data: [],
                colWidths: [25, 130, 550, 50, 65],
                manualColumnResize: true,
                colHeaders: ["id", "Kommune", "Beskrivelse", "Værdi", "Farve"],
                dataSchema: { id: null, title: null, html: null, value: null, color: null, path: null },
                columnSorting: true,
                columns: [
                  {
                      data: "id",
                  },
                  {
                      data: "title"
                  },
                  {
                      data: "html"
                  },
                  {
                      data: "value"
                  },
                  {
                    data : "color"
                  }
                ],
                onChange: function (data, source) {
                    if (source === 'loadData') {
                        return;
                    }
                    uMapDataHandler.setData(map);
                }
            });
            
            $dataTable.handsontable("loadData", map.settings.data);
        };

    var initUMaps = function (map) {


        if ($(map.hiddenSettingsId).val() != null && $(map.hiddenSettingsId).val() != "") {
            map.settings = $.parseJSON($(map.hiddenSettingsId).val());
        } else {
            $(map.hiddenSettingsId).val(JSON.stringify(map.settings));
        }

        $(window).on("change", map.legendSettingsClass, function () { uMapDataHandler.setLegends(this, map); });
        //$(window).on("change", chart.chartTypeClass, function () { uChartRenderHandler.redrawOrUpdate(this, chart) });

        initHandsonTable(map);
        console.log("Before initLegendsSettings");
        initLegendSettings(map);
        console.log("After initLegendsSettings");
    };

    return {
        initUMaps : initUMaps
    };


})();



}).call(this, window, jQuery);
