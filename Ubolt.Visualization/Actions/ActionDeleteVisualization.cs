﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco.interfaces;

namespace Ubolt.Visualization.Actions
{
    public class ActionDeleteVisualization : IAction 
    {
        private static readonly ActionDeleteVisualization _instance = new ActionDeleteVisualization();

        public static ActionDeleteVisualization Instance
        {
            get { return _instance; }
        }

        static ActionDeleteVisualization()
        {
            
        }

        #region IAction Members

        public string Alias
        {
            get { return "Slet Node"; }
        }

        public bool CanBePermissionAssigned
        {
            get { return true; }
        }

        public string Icon
        {
            get { return ".sprDelete"; }
        }

        public string JsFunctionName
        {
            get { return ""; }
        }

        public string JsSource
        {
            get { return "/javascript.js"; }
        }

        public char Letter
        {
            get { return 'q'; }
        }

        public bool ShowInNotifier
        {
            get { return false; }
        }

        #endregion
    }
}