﻿using umbraco.interfaces;

namespace Ubolt.Visualization.Tasks
{
    public class FolderTasks : ITaskReturnUrl
    {
        public string ReturnUrl { get; private set; }
        public int TypeID { get; set; }
        public int UserId { set; private get; }
        public string Alias { get; set; }
        public int ParentID { get; set; }
        
        public bool Delete()
        {
            return true;
        }

        public bool Save()
        {
           return false;
        }
 
    }
}