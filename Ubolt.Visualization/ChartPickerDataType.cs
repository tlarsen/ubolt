﻿using System;
using umbraco.cms.businesslogic.datatype;

namespace Ubolt.Visualization
{
    public class ChartPickerDataType : AbstractDataEditor
    {
        private readonly ChartPicker _control = new ChartPicker();

        public override Guid Id
        {
            get
            {
                return new Guid("31DF182B-C44C-4F7B-AF99-998E2B0BAE3F");
            }
        }

        public override string DataTypeName
        {
            get
            {
                return "Ubolt Chart Picker";
            }
        }

        public ChartPickerDataType()
        {
            this.RenderControl = _control;
            _control.Init += _control_Init;
            base.DataEditorControl.OnSave += DataEditorControl_OnSave;
        }

        void _control_Init(object sender, EventArgs e)
        {
            _control.DataValue = base.Data.Value != null ? base.Data.Value.ToString() : "";


            //if (base.Data.Value != null && base.Data.Value.ToString() != string.Empty)
            //{
            //    var model = Repositories.VisualizationChartRepository.Instance.Get(base.Data.Value.ToString());
            //    if (model != null)
            //        _control.Model = model;

            //}
        }

       

        protected void DataEditorControl_OnSave(EventArgs e)
        {
            //base.Data.Value = ((UI.ChartPickerControl)_control.Controls[0]).value.ToString();
            base.Data.Value = _control.DataValue;
        }


    }
}