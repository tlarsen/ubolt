﻿using umbraco.interfaces;

namespace Ubolt.Visualization
{
    public class ChartPickerMacroRender : ChartPicker, IMacroGuiRendering
    {

        private string _value = string.Empty;

        public bool ShowCaption
        {
            get { return false; }
        }

        public string Value
        {
            get { return ((UI.ChartPickerControl)Controls[0]).value.ToString(); }
            set { _value = value; }
        }

    }
}