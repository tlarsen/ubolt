﻿namespace Ubolt.Visualization.Models
{
    public class VisualizationPickerFilterModel
    {
        public string StartsWith { get; set; }
        public bool IncludeFolders { get; set; }
    }
}