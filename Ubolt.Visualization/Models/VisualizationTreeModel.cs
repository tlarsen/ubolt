﻿namespace Ubolt.Visualization.Models
{
    [TableName("UboltVisualizationNode")]
    [PrimaryKey("ID")]
    public class VisualizationTreeModel 
    {
        public int ID { get; set; }
        public int ParentID { get; set; }
        public string Template { get; set; }
        public string Name { get; set; }
    }
}
