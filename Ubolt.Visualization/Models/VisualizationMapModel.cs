﻿using System;
using Newtonsoft.Json;

namespace Ubolt.Visualization.Models
{
    [TableName("uboltVisualizationMap")]
    [PrimaryKey("ID")]
    public class VisualizationMapModel
    {
        public int ID { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
        
        [JsonProperty(PropertyName = "data")]
        public string Data { get; set; }
        
        public string Source { get; set; }
        public string Notes { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime CreateDate { get; set; }
        public int CreatedBy { get; set; }
        public int FkNodeId { get; set; }
    }
}