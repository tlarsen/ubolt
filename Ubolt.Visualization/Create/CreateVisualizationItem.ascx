﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateVisualizationItem.ascx.cs" Inherits="Ubolt.Visualization.Create.CreateVisualizationItem" %>
<input type="hidden" name="nodeType">

<div style="MARGIN-TOP: 20px">
    <%=umbraco.ui.Text("name")%>:<asp:RequiredFieldValidator id="RequiredFieldValidator1" ErrorMessage="*" ControlToValidate="tbName" runat="server">*</asp:RequiredFieldValidator><br />
    <asp:TextBox id="tbName" CssClass="bigInput" Runat="server" width="350px"></asp:TextBox>
    <!-- added to support missing postback on enter in IE -->
    <asp:TextBox runat="server" style="visibility:hidden;display:none;" ID="Textbox1"/>
</div>

<div style="MARGIN-TOP: 20px">Type:<br />
    <asp:DropDownList CssClass="bigInput" Runat="server" width="350px" ID="ddlTemplate">
        <asp:ListItem Text="Google chart" Value="visualizationChart" />
        <asp:ListItem Text="Map" Value="visualizationMap" />
        <asp:ListItem Text="Folder" Value="visualizationFolder" />
    </asp:DropDownList>
</div>

<div style="padding-top: 25px;">
	<asp:Button id="btnSubmit" Runat="server" style="Width:90px" onclick="btnSubmit_Click"></asp:Button>
	&nbsp; <em><%= umbraco.ui.Text("or") %></em> &nbsp;
  <a href="#" style="color: blue"  onclick="UmbClientMgr.closeModalWindow()"><%=umbraco.ui.Text("cancel")%></a>
</div>