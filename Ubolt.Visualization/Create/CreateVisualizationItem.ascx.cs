﻿using System;
using umbraco;
using umbraco.BasePages;

namespace Ubolt.Visualization.Create
{
    public partial class CreateVisualizationItem : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnSubmit.Text = ui.Text("create");
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int parentId = -1;
                if (helper.Request("nodeId") != "init")
                    parentId = int.Parse(helper.Request("nodeId"));

               
                //her skal der testes på hvad der oprettes 
                var nodeModel = Repositories.VisualizationTreeRepository.Instance.Create(tbName.Text, ddlTemplate.SelectedValue, parentId);

                switch (ddlTemplate.SelectedValue)
                {
                    case "visualizationChart":
                        Repositories.VisualizationChartRepository.Instance.Create(nodeModel.ID, nodeModel.Name);
                        break;
                    case "visualizationMap":
                        Repositories.VisualizationMapRepository.Instance.Create(nodeModel.ID, nodeModel.Name);
                        break;
                    default:
                        //Folder choosen dont insert 
                        break;
                }


                string returnUrl = GetReturnUrl(nodeModel.ID, nodeModel.Template);

                BasePage.Current.ClientTools.ReloadActionNode(false, true);
                BasePage.Current.ClientTools.CloseModalWindow();
                BasePage.Current.ClientTools.ChangeContentFrameUrl(returnUrl);
                BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.save, "Success", "Chart created");
            }

        }

        private static string GetReturnUrl(int id, string template)
        {
            string path;
            switch (template)
            {
                case "visualizationChart":
                    path = string.Format("/umbraco/plugins/ubolt/charts/ui/editors/EditGoogleChart.aspx?id={0}", id);
                    break;
                case "visualizationFolder":
                    path =  string.Format("/umbraco/plugins/ubolt/charts/ui/editors/ShowFolderContent.aspx?id={0}", id);
                    break;
                case "visualizationMap":
                    path = string.Format("/umbraco/plugins/ubolt/charts/ui/editors/EditMap.aspx?id={0}", id);
                    break;
                default:
                    path = string.Empty;
                    break;
            }

            return path;
        }
    }
}